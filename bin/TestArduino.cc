/*!
 * \authors Mattia Lizzo <mattia.lizzo@cern.ch>, INFN-Firenze
 * \authors Francesco Fiori <francesco.fiori@cern.ch>, INFN-Firenze
 * \authors Antonio Cassese <antonio.cassese@cern.ch>, INFN-Firenze
 * \date Sep 2 2019
 */

// Libraries
#include "Arduino.h"
#include "DeviceHandler.h"

#include <boost/program_options.hpp> //!For command line arg parsing
#include <fstream>
#include <iomanip>
#include <iostream>
#include <limits>

// Namespaces
namespace po = boost::program_options;

/*!
************************************************
* A simple "wait for input".
************************************************
*/
void wait()
{
    std::cout << "Press ENTER to continue...";
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}

/*!
************************************************
* Argument parser.
************************************************
*/
po::variables_map process_program_options(const int argc, const char* const argv[])
{
    po::options_description desc("Allowed options");

    desc.add_options()("help,h", "produce help message")

        ("config,c",
         po::value<std::string>()->default_value("default"),
         "set configuration file path (default files defined for each test) "
         "...")("verbose,v", po::value<std::string>()->implicit_value("0"), "verbosity level");

    po::variables_map vm;
    try
    {
        po::store(po::parse_command_line(argc, argv, desc), vm);
    }
    catch(po::error const& e)
    {
        std::cerr << e.what() << '\n';
        exit(EXIT_FAILURE);
    }
    po::notify(vm);

    // Help
    if(vm.count("help"))
    {
        std::cout << desc << "\n";
        exit(EXIT_SUCCESS);
    }

    // Power supply object option
    if(vm.count("object")) { std::cout << "Object to initialize set to " << vm["object"].as<std::string>() << std::endl; }

    return vm;
}

/*!
 ************************************************
 * Main.
 ************************************************
 */
int main(int argc, char* argv[])
{
    boost::program_options::variables_map v_map = process_program_options(argc, argv);
    std::cout << "Initializing ..." << std::endl;

    std::string        docPath = v_map["config"].as<std::string>();
    pugi::xml_document docSettings;

    DeviceHandler theHandler;
    theHandler.readSettings(docPath, docSettings);
    Arduino* kiraArduino;

    try
    {
        kiraArduino = theHandler.getArduino("MyArduino");
    }
    catch(const std::out_of_range& oor)
    {
        std::cerr << "Out of Range error: " << oor.what() << '\n';
        throw std::out_of_range(oor.what());
    }

    for(auto& status: kiraArduino->getStatusList()) { std::cout << status << ":\t" << kiraArduino->getParameterFloat(status) << std::endl; }

    std::cout << "Version: " << kiraArduino->getParameterString("version") << std::endl;

    // kiraArduino->getParameterString("trigger_on");
    kiraArduino->setParameter("trigger", "on");
    // kiraArduino->setParameter("frequency", 2000);

    // kiraArduino->setParameter("pulseLength",50);
    // std::cout << kiraArduino->getParameterString("pulseLength_100") << std::endl;

    // kiraArduino->setParameter("dacLed", "low");
    // kiraArduino->setParameter("led", "on", "Bot0");
    /*
    int count = 0;
    for (auto & component : kiraArduino->getComponentList())
    {
        std::cout << component << std::endl;
        kiraArduino->setParameter("intensity", 30000, component);


        if (count % 2 == 0)
        {
            kiraArduino->setParameter("led", "on", component);
        }
        else
        {
            kiraArduino->setParameter("led", "off", component);

        }
        count++;

        //kiraArduino->setParameter("led", "off", component);
    }
    */

    // kiraArduino->setParameter("led", "on", "Bot0");
    /*
    kiraArduino->setParameter("led", "off", "Top1");
    kiraArduino->setParameter("led", "off", "Top2");
    kiraArduino->setParameter("led", "off", "Top3");
    kiraArduino->setParameter("led", "off", "Top4");
    kiraArduino->setParameter("led", "off", "Top5");
    kiraArduino->setParameter("led", "off", "Top6");
    kiraArduino->setParameter("led", "off", "Top7");
    kiraArduino->setParameter("led", "off", "Bot0");
    kiraArduino->setParameter("led", "off", "Bot1");
    kiraArduino->setParameter("led", "off", "Bot2");
    kiraArduino->setParameter("led", "off", "Bot3");
    kiraArduino->setParameter("led", "off", "Bot4");
    kiraArduino->setParameter("led", "off", "Bot5");
    kiraArduino->setParameter("led", "off", "Bot6");
    kiraArduino->setParameter("led", "off", "Bot7");
    */
    /*
    for (auto & status : kiraArduino->getStatusList())
    {
        std::cout << status << ":\t" << kiraArduino->getParameterFloat(status) << std::endl;
    }
    */

    // kiraArduino->setParameter("led", "on", "Top7");
    /*
    temperature
    humidity
    dewpoint
    dacLed_high
    dacLed_low
    intensity_01_30000
    pulseLength_800
    frequency_1000
    trigger_on
    trigger_off
    */
    sleep(100);
    return 0;
}
