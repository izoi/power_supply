// Libraries
#include "DeviceHandler.h"
#include "PowerSupply.h"
#include "PowerSupplyChannel.h"
#include "IIHEPowerSupply.h"


#include <unistd.h>
#include <iostream>


// Namespaces
using namespace std;


int main(int argc, char* argv[])
{

    std::cout << "Initializing ..." << std::endl;
    pugi::xml_document docSettings;

    DeviceHandler theHandler;
    theHandler.readSettings("config/config_IIHE.xml", docSettings);
    auto psu = theHandler.getPowerSupply("IIHEPowerSupply")->getChannel("LV_1-0");
    psu->turnOn();
    for (int volt = 0; volt < 10; volt++){
        psu->setVoltage(volt);
        sleep(5);
        cout<<"V = "<< psu->getOutputVoltage()<<" I = "<<psu->getCurrent()<<endl;
    }

    psu->turnOff();
    psu->setVoltage(10.5);
    return 0;
}
