#!/usr/bin/env python3
import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import os, errno
import glob
import pathlib
import argparse
import xml.etree.ElementTree as ET
import csv
from analysis_functions import *

############
# Analysis #
############

def get_data(fileName, var):
    """Handle the files to extract the data of interest: total current, VinD,
    VddD, VinA, VddA
    """

    # Read data from file
    df = pd.read_csv(fileName)
    df = df.sort_values(by = ['CurrentD'], ascending = True)

    print("Analyzing file " + fileName)

    # Initialize variables
    valueList = [None] * len(var)
    IArray    = [None]

    # Get total current
    IArray = np.array(df['CurrentD'].tolist())

    # Get VinD, VddD, VinA, VddA
    for i in range(0,len(var)):
        valueList[i] = np.array(df[var[i]].tolist())

    return IArray, valueList
    
def VI_plotter(I,listVD,listVA,nCROCs,saveName,saveDir):
    """Perform a plot showing the digital and analog voltage as a function of
    the total current provided to the system. Save the plot as .png and .pdf
    """

    # Initialize variables
    vDPltList, vAPltList = ([None] * nCROCs for i in range(2))
    
    # Plot the digitial and analog voltages wrt the total current
    ax = plt.gca()
    for i in range (0,nCROCs):
        color = next(ax._get_lines.prop_cycler)['color']
        vDPltList[i] , = plt.plot(I, listVD[i], 'o', fillstyle="left", markeredgewidth=0.0, label = "{}D".format(saveName), alpha = 0.8, color=color)
        vAPltList[i] , = plt.plot(I, listVA[i], 'o', fillstyle="right", markeredgewidth=0.0, label = "{}A".format(saveName), alpha = 0.6, color=color)

    # Draw legend
    valuePltList = vDPltList + vAPltList
    plt.legend(handles = valuePltList)
    plt.legend(labelspacing = 0.3, ncol = 1, loc = 'upper left', bbox_to_anchor = (1.01,1.02))#framealpha = 0,)

    # Draw title and labels
    plt.xlabel('Total Input Current [A]')
    plt.ylabel('Voltage [V]')
    plt.title('IV curve')

    # Plot configuration
    figureWidth  = 8
    figureHeight = 4.8
    plt.gcf().set_size_inches(figureWidth, figureHeight)
    plt.tight_layout()
    plt.ylim([0,2.5])
    plt.grid()

    # Show and save plot
    print("Saving image: I{}D_I{}A.png".format(saveName,saveName))
    plt.savefig(saveDir+'I{}D_I{}A.pdf'.format(saveName,saveName), dpi = 1000)#, transparent = True)
    plt.savefig(saveDir+'I{}D_I{}A.png'.format(saveName,saveName), dpi = 1000)#, transparent = True)
    plt.close()

def main(args):
    """
    """
    
    # Extract the value of some parameters in the xml config file
    tree           = ET.parse(args.configuration)
    root           = tree.getroot()
    
    scannerCard    = root.find('ScannerCard')
    analysis       = root.find('Analysis')
    powerSupply    = root.find('Devices').find('PowerSupply')

    nCROCs         = int(analysis.attrib.get('nCROCs'))
    nVars          = int(scannerCard.attrib.get('nChannels'))

    # Variables
    var,labelArray = ([] for k in range(2))
    labelArray.append('D')
    labelArray.append('A')

    # Create variables to store the different voltages of all the CROCS
    listVinD, listVinA, listVddD, listVddA = ([] for k in range(4))

    # Get only the variables of interest: VinD/A and VddD/A
    for i in range(1,nVars+1):
        if ("Vin" in scannerCard.attrib.get('Channel_' + str(i)) or "Vdd" in scannerCard.attrib.get('Channel_' + str(i))):
            var.append(scannerCard.attrib.get('Channel_' + str(i)))
    
    folderForLoop = args.data

    fileSaveDir = folderForLoop + 'Images/'
    try:
        os.makedirs(fileSaveDir)
    except FileExistsError:
        pass

    # Find the files to be used for the plot
    fileList = glob.glob(folderForLoop + '**/lastScan.csv', recursive=True)

    # Get the data from each file
    for vFile in fileList:
        IArray, valueList = get_data(vFile, var)
        listVinD.append(valueList[0])
        listVddD.append(valueList[1])
        listVinA.append(valueList[2])
        listVddA.append(valueList[3])

    # Plot the data: VxxD/A vs Total Current
    VI_plotter(IArray,listVinD,listVinA,nCROCs,"Vin",fileSaveDir)
    VI_plotter(IArray,listVddD,listVddA,nCROCs,"Vdd",fileSaveDir)
    
##########
# Parser #
##########

parser = argparse.ArgumentParser(description = 'Plot of IV curve data using matplotlib and pandas')
parser.add_argument('-c', '--configuration', help = 'XML configuration file', default = 'iv_it_croc_sldo.xml', type = str)
parser.add_argument('-d', '--data', help = 'CSV results folder', default = '../results/', type = str)

args = parser.parse_args()

if __name__ == "__main__":
    main(args)
