#!/usr/bin/env python3
import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import os, errno
import glob
import pathlib
import argparse
import xml.etree.ElementTree as ET
import csv
from analysis_functions import *

############
# Analysis #
############

def fit_results_trace(channels, label, textstrArr, df, I, resultLine):
    """Keep track of the results from the IV curve fit
    """

    debuggingPrint('{} - retrieving offset and slope from string:', textstrArr, args.verbosity, 1)
    resultSlope = textstrArr.split('$')[1].split('=')[1]
    resultOff   = textstrArr.split('$')[3].split('=')[1]
    varNameRext = 'Vrext{}'.format(label)
    idx         = (np.abs(I - 0.9)).argmin()
    vRext       = (np.array(df[varNameRext].tolist()))[idx]
    resultLine.append(resultSlope)
    resultLine.append(resultOff)
    resultLine.append(vRext)

    return resultLine

def analyze(fileFolder, fileBaseName, minVoltageVal, maxVoltageVal, minCurrentVal, maxCurrentVal, var, twoChannelsFlag, nCROCs, labelArray, rExtArray, kFac, resultWriter, VinDid, VinAid):
    """Get, plot and fit the data (current and voltage). Also determine the overprotection voltage and current.
    """

    fileSaveDir     = fileFolder   + 'Images/'
    fileSaveName    = fileSaveDir  + fileBaseName
    fileSaveNameK   = fileSaveName + '_kfactor'
    fileName        = fileFolder   + fileBaseName + '.csv'

    resultLine      = []
    if resultWriter is not None:
        acqDayString    = (fileFolder.split(os.sep))[-2]
        acqDaySplitStr  = acqDayString.split('_')
        acqHourSplitStr = fileBaseName.split('_')
        acqDayAndTime   = acqDaySplitStr + acqHourSplitStr
        posixClass      = posixTS(acqDayAndTime)
        timeStamp       = posixClass.getTS()
        resultLine.append(timeStamp)

    try:
        os.makedirs(fileSaveDir)
    except FileExistsError:
        pass

    df = pd.read_csv(fileName)
    df = df.sort_values(by = ['CurrentD'], ascending = True)
    df = df.sort_values(by = ['CurrentA'], ascending = True)

    print("Analyzing file " + fileName)

    # Variables
    valueList                                                                     = [None] * (len(var) - 1)
    valuePltList                                                                  = [None] * len(valueList)
    IArray, VinPSArray, psPltArray, ivLinePltArray, ivShortPltArray, textstrArray = ([None] * 2 for i in range(6))
    currentOVP, voltageOVP                                                        = ([-1] * 2 for i in range(2))
    textstr                                                                       = ''
    I                                                                             = 0

    # Get data: current and voltage
    for v in range(2):
        varNameCurrent = 'Current' + labelArray[v]
        varNameVoltage = 'Vps'     + labelArray[v]
        varNameLabel   = 'VinPS'   + labelArray[v]
        IArray[v]      = np.array(df[varNameCurrent].tolist())
        IArray[v]      = IArray[v] / nCROCs
        VinPSArray[v]  = np.array(df[varNameVoltage].tolist())

        if not twoChannelsFlag:
            IArray[v] = np.divide(IArray[v],2)

        I = I + (IArray[v] / 2)

        # IV plot and fit when VinD or VinA is not read
        if (VinDid is None or VinAid is None):
            psPltArray[v] ,                                         = plt.plot(IArray[v], VinPSArray[v], 'o', label = varNameLabel, alpha = 0.5)
            ivLinePltArray[v], ivShortPltArray[v], textstrArray[v]  = ivCurveFit(IArray[v], VinPSArray[v], minVoltageVal, maxVoltageVal, minCurrentVal, maxCurrentVal, lineColor = psPltArray[v].get_color() , label = labelArray[v])
            textstr                                                 = textstr + textstrArray[v] + '\n'
            if (resultWriter is not None):
                txt_ch = "Single channel"
                resultLine = fit_results_trace(txt_ch, labelArray[v], textstrArray[v], df, IArray[v], resultLine)

    # IV plot when both VinD and VinA are read
    for i in range(1,len(var)):
        valueList[i-1] = np.array(df[var[i]].tolist())
    for i in range (0,len(valueList)):
        if var[i+1].endswith("D"):
            fillstyle = "left"
        elif var[i+1].endswith("A"):
            fillstyle = "right"
        else:
            fillstyle = "bottom"
        valuePltList[i] , = plt.plot(I, valueList[i], 'o', fillstyle=fillstyle, markeredgewidth=0.0, label = var[i+1], alpha = 0.8)

    # IV fit when both VinD and VinA are read
    if (VinDid is not None and VinAid is not None):
        # Digital
        ivLinePltArray[0], ivShortPltArray[0], textstrArray[0] = ivCurveFit(I, valueList[VinDid], minVoltageVal, maxVoltageVal, minCurrentVal, maxCurrentVal, lineColor = valuePltList[VinDid].get_color(), label = labelArray[0])
        textstr                                                = textstr + textstrArray[0] + '\n'
        if resultWriter:
            txt_ch = "Two channels"
            resultLine = fit_results_trace(txt_ch, labelArray[0], textstrArray[0], df, I, resultLine)
        ## VovpD calculation
        currentOVP[0], voltageOVP[0], resultLine = ovp_v_calculate(labelArray[0], textstrArray[0], valueList, currentOVP[0], voltageOVP[0], minVoltageVal, minCurrentVal, I, VinDid, VinAid, resultWriter, resultLine)

        # Analog
        ivLinePltArray[1], ivShortPltArray[1], textstrArray[1] = ivCurveFit(I, valueList[VinAid], minVoltageVal, maxVoltageVal, minCurrentVal, maxCurrentVal, lineColor = valuePltList[VinAid].get_color(), label = labelArray[1])
        textstr                                                = textstr + textstrArray[1]
        if resultWriter:
            txt_ch = "Two channels"
            resultLine = fit_results_trace(txt_ch, labelArray[1], textstrArray[1], df, I, resultLine)
        ## VovpA calculation
        currentOVP[1], voltageOVP[1], resultLine = ovp_v_calculate(labelArray[1], textstrArray[1], valueList, currentOVP[1], voltageOVP[1], minVoltageVal, minCurrentVal, I, VinDid, VinAid, resultWriter, resultLine)

    # Draw OVP line
    if (I[0] < 0.2 and currentOVP[0] != -1):
        print("Writing ovp on plot")
        plt.axhline(y = voltageOVP[0], color = 'r', linestyle = '--')
        plt.axvline(x = currentOVP[0], color = 'r', linestyle = '--')
        plt.text(0.2, voltageOVP[0], 'OVP: ' + '{:.2f}'.format(voltageOVP[0]) + ' V', fontsize = 10, color = 'r', va = 'center', ha = 'center', backgroundcolor = 'w')

    # Draw legend
    for vIndex, vLinePlt in enumerate(ivLinePltArray):
        valuePltList.append(ivLinePltArray[vIndex])
        valuePltList.append(ivShortPltArray[vIndex])
    plt.legend(handles = valuePltList)
    plt.legend(labelspacing = 0.3, ncol = 1, loc = 'lower left', bbox_to_anchor = (1.01,-0.02))#framealpha = 0,)

    # Draw fit box
    yMin = -0.1
    yMax = 2.5
    xMin = 0.0
    xMax = 2.1
    ax = plt.gca()
    ax.set_ylim(yMin,yMax)
    ax.set_xlim(xMin,xMax)
    props = dict(boxstyle = 'round', facecolor = 'green', alpha = 0.2)
    ax.text(1.035, 0.985, textstr, transform = ax.transAxes, fontsize = 13, horizontalalignment = 'left', verticalalignment = 'top', bbox = props)

    # Draw title and labels
    plt.xlabel('Input Current per Channel [A]')
    plt.ylabel('Voltage [V]')
    plt.title('IV curve')

    # Plot configuration
    figureWidth  = 8
    figureHeight = 4.8
    plt.gcf().set_size_inches(figureWidth, figureHeight)
    plt.tight_layout()
    plt.ylim([0,2.5])
    plt.grid()

    # Show and save plot
    print("Saving image: " + fileSaveName)
    plt.savefig(fileSaveName + '.pdf', dpi = 1000)#, transparent = True)
    plt.savefig(fileSaveName + '.png', dpi = 1000)#, transparent = True)
    #plt.show()
    plt.close()

    # Calculate k factor
    if (kFac and VinDid is not None and VinAid is not None):# and resultWriter is not None):
        kFactorArray, vIn, vRext, kPltArray, k = ([None] * 2 for i in range(5))

        for v in range(2):
            varNameVoltage = 'Vin'   + labelArray[v]
            varNameRext    = 'Vrext' + labelArray[v]
            current        = IArray[v]
            vIn            = np.array(df[varNameVoltage].tolist())
            vRext          = np.array(df[varNameRext].tolist())
            k[v]           = []
            for u in range(0,len(current)):
                k[v].append((current[u] * rExtArray[v]) / (vIn[u] - vRext[u]))

            plt.figure()
            kPltArray[v] , = plt.plot(IArray[v], k[v], 'o', label = 'K - ' + labelArray[v])

            # Draw title and labels
            plt.xlabel('Input Current ' + labelArray[v] + ' [A]')
            plt.ylabel('K Factor '      + labelArray[v])
            plt.title ('K Factor '      + labelArray[v])

            # Plot configuration
            plt.gcf().set_size_inches(figureWidth, figureHeight)
            plt.tight_layout()
            plt.ylim([0,5000])
            plt.grid()

            # Save plot
            print("Saving image: " + fileSaveNameK + labelArray[v])
            plt.savefig(fileSaveNameK + labelArray[v] + '.pdf', dpi = 1000)#, transparent = True)
            plt.savefig(fileSaveNameK + labelArray[v] + '.png', dpi = 1000)#, transparent = True)
            plt.close()

    if resultWriter:
        resultWriter.writerow(resultLine)

########
# Main #
########

def main(args):

    tree        = ET.parse(args.configuration)
    root        = tree.getroot()

    scannerCard = root.find('ScannerCard')

    var         = []
    i           = 1

    var.append(scannerCard.attrib.get('nChannels'))

    rextExists, j  = (0 for k in range(2))
    VinDid, VinAid = (None for k in range(2))

    for i in range(1,int(var[0])+1):
        if scannerCard.attrib.get('Channel_' + str(i)):
            var.append(scannerCard.attrib.get('Channel_' + str(i)))
            j += 1
            if scannerCard.attrib.get('Channel_' + str(i)) == "VinD":
                VinDid = j - 1
            if scannerCard.attrib.get('Channel_' + str(i)) == "VinA":
                VinAid = j - 1
            if scannerCard.attrib.get('Channel_' + str(i)) == "VrextD":
                rextExists += 0.5
            if scannerCard.attrib.get('Channel_' + str(i)) == "VrextA":
                rextExists += 0.5

    if (VinDid is None or VinAid is None):
        print("\nWARNING: No VinD/A provided with the IV scan! Fits will be performed on VinPSD/A. OVP computation NOT available. Kfactor computation NOT available.\n")
    if (rextExists < 1):
        print("\nWARNING: No VrextD/A provided with the IV scan! Fit data will NOT be saved. Kfactor computation NOT available.\n")

    analysis            = root.find('Analysis')
    powerSupply         = root.find('Devices').find('PowerSupply')
    nCROCs              = int(analysis.attrib.get('nCROCs'))
    rExtD               = float(analysis.attrib.get('RextD'))
    rExtA               = float(analysis.attrib.get('RextA'))
    minVoltageVal       = float(analysis.attrib.get('minVoltageVal'))
    maxVoltageVal       = float(analysis.attrib.get('maxVoltageVal'))
    minCurrentVal       = float(analysis.attrib.get('minCurrentVal'))
    maxCurrentVal       = float(analysis.attrib.get('maxCurrentVal'))
    kFac                = str_to_bool(analysis.attrib.get('kFactor'))
    read                = str_to_bool(analysis.attrib.get('ReadEverything'))
    two                 = str_to_bool(powerSupply.attrib.get('TwoChannels'))

    folderForLoop       = args.data
    saveResultsFileName = folderForLoop + 'Results.csv'

    if (os.path.isfile(saveResultsFileName)):
        os.remove(saveResultsFileName)

    vFileRes     = open(saveResultsFileName, 'a')
    resultWriter = csv.writer(vFileRes, delimiter = ',', quotechar = '"', quoting = csv.QUOTE_MINIMAL)

    if read:
        fileList = glob.glob(folderForLoop + '[!imux][!Results]*.csv')
    else:
        fileList = glob.glob(folderForLoop + 'lastScan.csv')
    debuggingPrint('File list:', fileList, args.verbosity)

    labelArray, rExtArray, headerLine = ([] for k in range(3))

    labelArray.append('D')
    labelArray.append('A')

    rExtArray.append(rExtD)
    rExtArray.append(rExtA)

    headerLine.append('TimeStamp')
    headerLine.append('SlopeD')
    headerLine.append('OffsetD')
    headerLine.append('VrextD')
    if (VinDid is not None and VinAid is not None):
        headerLine.append('Iovp')
        headerLine.append('VovpD')
    headerLine.append('SlopeA')
    headerLine.append('OffsetA')
    headerLine.append('VrextA')
    if (VinDid is not None and VinAid is not None):
        headerLine.append('VovpA')
    resultWriter.writerow(headerLine)

    for vFile in fileList:
        fileBaseName = pathlib.Path(vFile).stem
        if fileBaseName == 'lastScan':
            resultWriterTmp = None
        elif rextExists < 1:
            resultWriterTmp = None
        elif rextExists == 1:
            resultWriterTmp = resultWriter
        analyze(folderForLoop, fileBaseName, minVoltageVal, maxVoltageVal, minCurrentVal, maxCurrentVal, var, two, nCROCs, labelArray, rExtArray, kFac, resultWriterTmp, VinDid, VinAid)

    if vFileRes is not None:
        vFileRes.close()

###################
# Argument parser #
###################

parser = argparse.ArgumentParser(description = 'Plot of IV curve data using matplotlib and pandas')
parser.add_argument('-c', '--configuration', help = 'XML configuration file' , default = '../config/iv_it_croc_sldo.xml', type = str)
parser.add_argument('-d', '--data'         , help = 'CSV results folder'     , default = '../results/SP_Try/2022_04_04/', type = str)
parser.add_argument('-v', '--verbosity'    , help = 'Verbosity for debugging', default = 0                              , type = int)

args = parser.parse_args()

if __name__ == "__main__":
    main(args)
