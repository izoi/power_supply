#!/usr/bin/env python3
import pandas as pd
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import os, errno
import glob
import pathlib
import argparse
from datetime import datetime
import glob
#import warnings
#from PyPDF2 import PdfMerger as merger
#import sys
#sys.path.append('./utils')
from analysis_functions import *

########
# Main #
########
def main(args):
    irradDict = plotvsDose(args)
    plotAllDoses(args,irradDict)

################################
# Plot all lines for all doses #
################################
def plotAllDoses(args,irradDict):
    figureWidth    = 8
    figureHeight   = 4.8
    markLineRext   = 0.9
    labelArray     = ['D','A']
    figureArray    = ['IV','VDD', 'Vrext']
    # figureArray    = ['IV','VDD','Vref','VoffHalf']
    files          = []
 
    norm           = mpl.colors.Normalize(vmin = 0, vmax = 1000)
    cmap           = mpl.cm.ScalarMappable(norm = norm, cmap = mpl.cm.nipy_spectral)
    cmap.set_array([])
 
    saveFigPath    = os.path.join(args.data,'Images')

    if not os.path.isdir(saveFigPath):
        os.mkdir(saveFigPath)
    for vDir in os.listdir(args.data):
        if vDir == 'Images' or vDir == 'Old':
            continue
        fullPath = os.path.join(args.data,vDir)
        debuggingPrint('Full path:', fullPath, args.verbosity,1)
        if not os.path.isdir(fullPath):
            continue
        tmpFiles  = glob.glob(os.path.join(fullPath,'*_' + args.baseName + '.csv'))
        for vTmpFile in tmpFiles:
            if not 'imux' in vTmpFile:
                files.append(vTmpFile)
    debuggingPrint('Files:', files, args.verbosity,1)
    for v, vFile in enumerate(files):
        df = pd.read_csv(vFile, comment = '#')
        acqDayString    = (vFile.split(os.sep))[-2]
        fileBaseName    = (vFile.split(os.sep))[-1]
        acqDaySplitStr  = acqDayString.split('_')
        acqHourSplitStr = fileBaseName.split('_')
        debuggingPrint('Data:', df, args.verbosity)

        acqDayAndTime = acqDaySplitStr + acqHourSplitStr 
        posixClass    = posixTS(acqDayAndTime)
        timeStamp     = posixClass.getTS()
        try:
            irradiation   = irradDict[timeStamp]
        except:
            print('Skipping timestamp %s', timeStamp)
            continue

        # Getting data
        for vLabel in labelArray:
            varNameCurrent = 'Current' + vLabel
            varNameVoltage = 'Vps'     + vLabel
            varNameVout    = 'Vdd'     + vLabel
            varNameRext    = 'Vrext'   + vLabel
            try:
                I              = np.array(df[varNameCurrent].tolist())
            except:
                raise Exception("\nMissing "+varNameCurrent+" from .csv files\n")
            try:
                VinPS          = np.array(df[varNameVoltage].tolist())
            except:
                raise Exception("\nMissing "+varNameVoltage+" from .csv files\n")
            try:
                Vout           = np.array(df[varNameVout]   .tolist())
            except:
                raise Exception("\nMissing "+varNameVout+" from .csv files\n")
            try:
                Rext           = np.array(df[varNameRext]   .tolist()) 
            except:
                raise Exception("\nMissing "+varNameRext+" from .csv files\n")
            # Figures
            plt.figure(figureArray[0] + vLabel)
            plt.plot(I,VinPS, c = cmap.to_rgba(irradiation))
            plt.figure(figureArray[1] + vLabel)
            plt.plot(I,Vout, c = cmap.to_rgba(irradiation))
            plt.figure(figureArray[2] + vLabel)
            plt.plot(I,Rext, c = cmap.to_rgba(irradiation))
            plt.axvline(x = markLineRext , color = 'r', linestyle='--')

    print()
    pdfMergeFile = PdfPages(os.path.join(saveFigPath,'summaryAllDoses.pdf'))
    for vLabel in labelArray:
        for vFigBaseName in figureArray:
            vFig = vFigBaseName + vLabel
            plt.figure(vFig)
            plt.xlabel('Input Current [A]')
            plt.ylabel('Voltage [V]')
            #plt.zlabel('Dose [Mrad]')
            plt.title(vFig)
            plt.gcf().set_size_inches(figureWidth, figureHeight)
            plt.tight_layout()
            plt.grid()
            colorBar    = plt.colorbar(cmap)
            #colorBar.set_label('Dose [Mrad]')
            colorBar.ax.set_title('Dose [Mrad]')
            saveFigName = os.path.join(saveFigPath,vFig)
            print(saveFigName)
            plt.savefig(saveFigName + '.pdf'                    , dpi = 1000)
            plt.savefig(saveFigName + '.png', transparent = True, dpi = 1000)
            plt.savefig(pdfMergeFile, format = 'pdf', dpi = 1000)
    pdfMergeFile.close()

########################
# Plots with Dose in x #
########################
def plotvsDose(args):
    resultFileList    = []
    slopeArray        = []
    offsetArray       = []
    irrArray          = []
    irrTimeStampArray = []
    figureWidth       = 8
    figureHeight      = 4.8
    markLine          = 300
    labelArray        = ['D','A']
    figureArray       = ['Irradiation','Slope', 'Offset','OffsetAll']
    irradDict         = {}
    
    saveFigPath    = os.path.join(args.data,'Images')
    irraConfigFile = os.path.join(args.data,args.irrConf)

    if not os.path.isfile(irraConfigFile):
        raise NameError('Configuration file for irradiation ' + irraConfigFile + ' not found')

    if not os.path.isdir(saveFigPath):
        os.mkdir(saveFigPath)
    for vDir in os.listdir(args.data):
        if vDir == 'Images' or vDir == 'Old':
            continue
        fullPath = os.path.join(args.data,vDir)
        if not os.path.isdir(fullPath):
            continue
        fileName = os.path.join(fullPath, args.file + '.csv')
        if not os.path.isfile(fileName):
            print('Warning: File ' + fileName + ' does not exist')
        else:
            resultFileList.append(fileName)

    dfData            = pd.concat((pd.read_csv(f) for f in resultFileList))
    dfData            = dfData.sort_values(by = 'TimeStamp', ascending = True)
    dfIrra            = pd.read_csv(irraConfigFile, comment = '#')
    #debuggingPrint(string = 'Irradiation summary:', variable = dfIrra, verbosityValue = args.verbosity, verbosityLevel = 0)
    debuggingPrint('Irradiation summary:', dfIrra, args.verbosity)
    debuggingPrint('Irradiation data:'   , dfData, args.verbosity)

    for vInd, vRow in dfIrra.iterrows():
        DayAndTime  = vRow.tolist()
        posixClass  = posixTS(DayAndTime)
        timeStamp   = posixClass.getTS()
        irrTimeStampArray.append(timeStamp)

    irrTimeStamp    = np.array(irrTimeStampArray)
    irrSlope        = np.array(dfIrra['Slope'].tolist())
    timeStampOffset = np.array(dfData['TimeStamp'].tolist())
    timeStamp       = timeStampOffset - irrTimeStamp[0]
    irrTimeStamp    = irrTimeStamp    - irrTimeStamp[0]
    debuggingPrint('Time stamp:', timeStamp, args.verbosity, 1)
    debuggingPrint('Irradiation time stamp:', irrTimeStamp, args.verbosity, 1)
    for v in range(0,len(timeStamp)):
        irrArray.append(0)
        for u in range(0,len(irrTimeStamp)):
            if timeStamp[v] > irrTimeStamp[u+1]:
                irrArray[v] = irrArray[v] + (irrTimeStamp[u+1] - irrTimeStamp[u])*irrSlope[u]
            else:
                irrArray[v] = irrArray[v] + (timeStamp[v]      - irrTimeStamp[u])*irrSlope[u]
                irrArray[v] = irrArray[v] / 3600 # Slope is in Mrad/h, timestamp is in seconds
                irradDict[timeStampOffset[v]] = irrArray[v]
                break
    irradiation = np.array(irrArray)
    debuggingPrint('Irradiation array:', irradiation, args.verbosity)

    plt.figure('Irradiation')
    plt.plot(irradiation, timeStamp , 's-', markersize = 3, label = 'Irradiation vs. time' ) 
    plt.xlabel('Dose [Mrad]')
    plt.ylabel('Time stamp')
    plt.title ('Time vs. irradiation')

    for vLabel in labelArray:
        slope  = np.array(dfData['Slope'  + vLabel].tolist())
        offset = np.array(dfData['Offset' + vLabel].tolist())
        rext   = np.array(dfData['Vrext' + vLabel].tolist())
        slopeArray.append(slope)
        offsetArray.append(offset)

        plt.figure(figureArray[1])
        plt.plot(irradiation, slope , 's-', markersize = 3, label = 'Exctracted Slope '  + vLabel)
        plt.figure(figureArray[2])
        plt.plot(irradiation, offset, 's-', markersize = 3, label = 'Exctracted Offset ' + vLabel)
        plt.figure(figureArray[3])
        plt.plot(irradiation, offset, 's-', markersize = 3, label = 'Exctracted Offset ' + vLabel)
        plt.plot(irradiation, rext  , 's-', markersize = 3, label = 'Vrext ' + vLabel + ' @900mA')

    plt.figure('Slope')
    plt.xlabel('Dose [Mrad]')
    plt.ylabel('IV exctracted slope [V/A]')
    plt.title ('IV exctracted slope')

    plt.figure('Offset')
    plt.xlabel('Dose [Mrad]')
    plt.ylabel('IV exctracted offset [V]')
    plt.title ('IV exctracted offset')

    plt.figure('OffsetAll')
    plt.xlabel('Dose [Mrad]')
    plt.ylabel('IV offset [V]')
    plt.title ('IV offset')

    # Showing and saving
    print()
    pdfMergeFile = PdfPages(os.path.join(saveFigPath,'summaryPlotVsDose.pdf'))
    for vFigName in figureArray:
        plt.figure(vFigName)
        plt.axvline(x = markLine , color = 'r', linestyle='--')
        plt.xlim(-10,1000)
        plt.grid(which='minor', color='#EEEEEE', linestyle=':', linewidth=0.5)
        plt.minorticks_on()
        plt.legend()
        plt.gcf().set_size_inches(figureWidth, figureHeight)
        plt.tight_layout()
        plt.grid()
        saveFigName = os.path.join(saveFigPath,vFigName)
        print(saveFigName)
        plt.savefig(saveFigName + '.pdf'                    , dpi = 1000)
        plt.savefig(saveFigName + '.png', transparent = True, dpi = 1000)
        plt.savefig(pdfMergeFile, format = 'pdf', dpi = 1000)
    pdfMergeFile.close()

    debuggingPrint('Irradiation dictionary:', irradDict, args.verbosity, 1)
    return irradDict



###################
# Argument parser #
###################
parser = argparse.ArgumentParser(description='Plot irradiation test summary plot of IV curve data using matplotlib and pandas')
parser.add_argument('-d', '--data'     , help = 'Data folder'                   , default = './results/201A2_TEST' , type = str)
parser.add_argument('-f', '--file'     , help = 'Base csv file name for results', default = 'Results'              , type = str)
parser.add_argument('-c', '--irrConf'  , help = 'Irradiation info csv file name', default = 'irradiationConfig.csv', type = str)
parser.add_argument('-b', '--baseName' , help = 'Base name for single acq data' , default = 'CROC_FI'              , type = str)
parser.add_argument('-v', '--verbosity', help = 'Verbosity for debugging'       , default = 0                      , type = int)

args = parser.parse_args()

if __name__ == "__main__":
    main(args)
