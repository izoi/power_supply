#!/usr/bin/env python3
import argparse
#import sys
#sys.path.append('./utils')

from tcp_utils import tcp_util

#BUFFER_SIZE = 1024

parser = argparse.ArgumentParser(description='Script to send specific commands to PoewerSupplyConnection.')
parser.add_argument('-i', '--ip'       , help = 'TCP IP'              , default = '0.0.0.0'      , type = str  )
parser.add_argument('-p', '--port'     , help = 'TCP PORT'            , default = 7000           , type = int  )
parser.add_argument('-m', '--message'  , help = 'Message to be sent'  , default = 'Hello, world!', type = str  )

args = parser.parse_args()

def main(args):
    """Send command on TCP socket
    """
    tcpClass = tcp_util(args.ip,args.port)
    tcpClass.sendMessage(args.message)

    #data = s.recv(BUFFER_SIZE)

    #print(data)
    pass

if __name__ == "__main__":
    main(args)
