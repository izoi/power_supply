/*!
 * \authors Stefan Maier <s.maier@kit.edu>, KIT-Karlsruhe
 * \authors Mattia Lizzo <mattia.lizzo@cern.ch>, INFN-Firenze
 * \authors Francesco Fiori <francesco.fiori@cern.ch>, INFN-Firenze
 * \authors Antonio Cassese <antonio.cassese@cern.ch>, INFN-Firenze
 * \date Feb 1 2022
 */

#ifndef Arduino_H
#define Arduino_H
#include "Device.h"
#include "pugixml.hpp"
#include <iostream>
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>

/*!
************************************************
 \class Arduino.
 \brief Abstract Arduino class
************************************************
*/

class Arduino : public Device
{
  public:
    Arduino(std::string model, const pugi::xml_node configuration);
    virtual ~Arduino(void);
    std::string convertToLFCR(std::string);
    // Virtual methods
    virtual void configure(void) = 0;

    // With the component string subparts such as LEDs of the KIRA Arduino can be acessed

    virtual void setParameter(std::string parameter, float value, std::string component = "");
    // virtual void setParameter(std::string parameter, bool value,        std::string component = "");
    virtual void setParameter(std::string parameter, int value, std::string component = "");
    virtual void setParameter(std::string parameter, std::string value, std::string component = "");

    virtual float       getParameterFloat(std::string parameter, std::string component = "");
    virtual int         getParameterInt(std::string parameter, std::string component = "");
    virtual bool        getParameterBool(std::string parameter, std::string component = "");
    virtual std::string getParameterString(std::string parameter, std::string component = "");

    virtual std::vector<std::string> getStatusList(void) const;
    virtual std::vector<std::string> getComponentList(void) const;
    std::string                      getModel() const;
    std::string                      getID() const;

  protected:
    std::string          fId;
    const std::string    fModel;
    const pugi::xml_node fConfiguration;
};

#endif
