#include "Device.h"
#include <iostream>
//#include <string>
#include <sstream>
#include <stdexcept>
/*!
************************************************
* Device constructor.
************************************************
*/
Device::Device(void) {}

/*!
************************************************
* Device distructor.
************************************************
*/
Device::~Device(void) {}

bool Device::vReadQ(void)
{
    std::cout << "This method was not implemented for this device!" << std::endl;
    return false;
}

bool Device::iReadQ(void)
{
    std::cout << "This method was not implemented for this device!" << std::endl;
    return false;
}

bool Device::rReadQ(void)
{
    std::cout << "This method was not implemented for this device!" << std::endl;
    return false;
}

float Device::vRead(void)
{
    std::stringstream error;
    error << "Sorry, this method was not implemented for this device! "
             "skipping the command ...";
    throw std::runtime_error(error.str());
}

float Device::iRead(void)
{
    std::stringstream error;
    error << "Sorry, this method was not implemented for this device! "
             "skipping the command ...";
    throw std::runtime_error(error.str());
}

float Device::rRead(void)
{
    std::stringstream error;
    error << "Sorry, this method was not implemented for this device! "
             "skipping the command ...";
    throw std::runtime_error(error.str());
}