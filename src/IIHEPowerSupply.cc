#include "IIHEPowerSupply.h"
#include <iostream>

#ifdef ZMQ_FOUND
    #include <zmq.h>
#endif


ZMQConnectionBase::ZMQConnectionBase(const std::string & host, int port){
    connect(host,port);
}

bool ZMQConnectionBase::connect(const std::string & host, int port){
    fHost = host;
    fPort = port;
    #ifdef ZMQ_FOUND
        fContext = zmq_ctx_new ();
        fRequester = zmq_socket (fContext, ZMQ_REQ);
        zmq_connect (fRequester, ("tcp://"+host+":"+std::to_string(port)).c_str());
        fIsConnected = true;
        if (request("ping") != "pong"){
            fIsConnected = false;
            std::cerr<<"Fatal error, server not responding..."<<std::endl;
        }
        return fIsConnected;
    #else
        std::cout<<"Package compiled without ZMQ, unable to connect!"<<std::endl;
        return 0;
    #endif
}

ZMQConnectionBase::ZMQConnectionBase(){
    fIsConnected = false;
    fContext = nullptr;
    fRequester = nullptr;
}

ZMQConnectionBase::~ZMQConnectionBase(){
    #ifdef ZMQ_FOUND
        zmq_close (fRequester);
        zmq_ctx_destroy (fContext);
    #endif
}

std::string ZMQConnectionBase::request(const std::string & command){
    if (fIsConnected == false){
        std::cerr<<"Connection isn't running!"<<std::endl;
        return "";
    }

    //clear buffer
    for (int i = 0; i < IIHE_ZMQ_BUFFER_SIZE; i++)
        fBuffer[i] = 0;
    
    #ifdef ZMQ_FOUND
        zmq_send (fRequester, command.c_str(), command.size(), 0);
        zmq_recv (fRequester, fBuffer, IIHE_ZMQ_BUFFER_SIZE, 0);
    #endif

    return std::string(fBuffer);
}

bool ZMQConnectionBase::isConnected(){
    return fIsConnected;
}

ZMQConnection::ZMQConnection(const std::string & host, int port){
    ZMQConnectionBase cb = ZMQConnectionBase(host,port);
    int new_port = std::stoi(cb.request("new_connection"));
    fConnection.connect(host,new_port);
    std::cout<<"Connection established to "<<host<<":"<<port<<std::endl;
    fIsConnected = true;
}

ZMQConnection::~ZMQConnection(){}

bool ZMQConnection::isConnected(){
    return (fIsConnected && fConnection.isConnected());
}

std::string ZMQConnection::request(const std::string & command){
    if (fIsConnected == false || fConnection.isConnected() == false){
        return "";
    }
    return fConnection.request(command);
}

IIHEPowerSupply::IIHEPowerSupply(const pugi::xml_node configuration) : PowerSupply("IIHE", configuration){
    fConnection = new ZMQConnection("localhost",5555);
    configure();
}

void IIHEPowerSupply::configure()
{
    for(pugi::xml_node channel = fConfiguration.child("Channel"); channel; channel = channel.next_sibling("Channel"))
    {
        std::string inUse = channel.attribute("InUse").value();
        if(inUse.compare("Yes") != 0 && inUse.compare("yes") != 0) continue;
        std::string id = channel.attribute("ID").value();
        PowerSupply::fChannelMap.emplace(id, new IIHEPowerSupplyChannel(fConnection, channel));
    }
}

IIHEPowerSupply::~IIHEPowerSupply(){
    delete fConnection;
}

IIHEPowerSupplyChannel::IIHEPowerSupplyChannel(ZMQConnection* connection, const pugi::xml_node configuration):PowerSupplyChannel(configuration){
    fConnection = connection;
    fPrefix     = configuration.attribute("HVLV").value();
    fChannelId  = configuration.attribute("Channel").value();
}

IIHEPowerSupplyChannel::~IIHEPowerSupplyChannel(){}

void IIHEPowerSupplyChannel::turnOn(void){
    fConnection->request(fPrefix+" turn_on "+fChannelId);
}

void IIHEPowerSupplyChannel::turnOff(void){
    fConnection->request(fPrefix+" turn_off "+fChannelId);
}

bool IIHEPowerSupplyChannel::isOn(void){
    return std::stoi(fConnection->request(fPrefix+" is_enabled "+fChannelId));
}

void IIHEPowerSupplyChannel::setVoltage(float voltage){
        fConnection->request(fPrefix + " set_v " + std::to_string(voltage) + " " + fChannelId);
}
void IIHEPowerSupplyChannel::setCurrent(float current){
    std::cerr<<"Not implemented on this PSU"<<std::endl;
}
void IIHEPowerSupplyChannel::setVoltageCompliance(float voltage){
    std::cerr<<"Not implemented on this PSU"<<std::endl;
}
void IIHEPowerSupplyChannel::setCurrentCompliance(float current){
    std::cerr<<"Not implemented on this PSU"<<std::endl;
}
void IIHEPowerSupplyChannel::setOverVoltageProtection(float voltage){
    std::cerr<<"Not implemented on this PSU"<<std::endl;
}
void IIHEPowerSupplyChannel::setOverCurrentProtection(float current){
    std::cerr<<"Not implemented on this PSU"<<std::endl;
}

float IIHEPowerSupplyChannel::getSetVoltage(){
    return std::stof(fConnection->request(fPrefix+" get_v_set "+fChannelId));
}
float IIHEPowerSupplyChannel::getOutputVoltage(){
    return std::stof(fConnection->request(fPrefix+" get_voltage "+fChannelId));
}
float IIHEPowerSupplyChannel::getCurrent(){
    return std::stof(fConnection->request(fPrefix+" get_current "+fChannelId));

}
float IIHEPowerSupplyChannel::getVoltageCompliance(){
    std::cerr<<"Not implemented on this PSU"<<std::endl;
    return 0;
}

float IIHEPowerSupplyChannel::getCurrentCompliance(){
    std::cerr<<"Not implemented on this PSU"<<std::endl;
    return 0;
}

float IIHEPowerSupplyChannel::getOverVoltageProtection(){
    std::cerr<<"Not implemented on this PSU"<<std::endl;
    return 0;
}
float IIHEPowerSupplyChannel::getOverCurrentProtection(){
    std::cerr<<"Not implemented on this PSU"<<std::endl;
    return 0;
}
void  IIHEPowerSupplyChannel::setParameter(std::string parName, float value){
    std::cerr<<"Not implemented on this PSU"<<std::endl;
}

void  IIHEPowerSupplyChannel::setParameter(std::string parName, bool value){
    std::cerr<<"Not implemented on this PSU"<<std::endl;
}
void  IIHEPowerSupplyChannel::setParameter(std::string parName, int value){
    std::cerr<<"Not implemented on this PSU"<<std::endl;

}
float IIHEPowerSupplyChannel::getParameterFloat(std::string parName){
    std::cerr<<"Not implemented on this PSU"<<std::endl;
    return 0;
}
int   IIHEPowerSupplyChannel::getParameterInt(std::string parName){
    std::cerr<<"Not implemented on this PSU"<<std::endl;
    return 0;
}
bool  IIHEPowerSupplyChannel::getParameterBool(std::string parName){
    std::cerr<<"Not implemented on this PSU"<<std::endl;
    return 0;
}
