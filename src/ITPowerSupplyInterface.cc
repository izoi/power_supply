#include <ctime>
#include <iostream>
#include <stdio.h>
#include <unistd.h>

#include "ITPowerSupplyInterface.h"
#include "TTi.h"
//========================================================================================================================
ITPowerSupplyInterface::ITPowerSupplyInterface(int serverPort, std::string configFileName) : TCPServer(serverPort, 10) { fHandler.readSettings(configFileName, fDocSettings); }

ITPowerSupplyInterface::ITPowerSupplyInterface(int serverPort) : TCPServer(serverPort, 10) { std::cout << "Ready to receive datas!" << std::endl; }

//========================================================================================================================
ITPowerSupplyInterface::~ITPowerSupplyInterface(void) { std::cout << __PRETTY_FUNCTION__ << " DESTRUCTOR" << std::endl; }

//========================================================================================================================
std::string ITPowerSupplyInterface::interpretMessage(const std::string& buffer)
{
    std::lock_guard<std::mutex> theGuard(fMutex);

    std::cout << __PRETTY_FUNCTION__ << " Message received from Ph2ACF: " << buffer << std::endl;
    std::map<std::string, std::string> valuesMap;
    try
    {
        valuesMap = createVariablesMap(buffer);
    }
    catch(const std::runtime_error& rte)
    {
        std::cout << "Entering error" << std::endl;
        std::cerr << rte.what() << '\n';
        throw std::runtime_error(rte.what());
    }
    if(buffer == "Initialize") // Changing the status changes the mode in
                               // threadMain (BBC) function
    { return "InitializeDone"; }
    else if(buffer.substr(0, 5) == "Start") // Changing the status changes the
                                            // mode in threadMain (BBC)
                                            // function
    {
        return "StartDone";
    }
    else if(buffer.substr(0, 4) == "Stop")
    {
        return "StopDone";
    }
    else if(buffer.substr(0, 4) == "Halt")
    {
        return "HaltDone";
    }
    else if(buffer == "Pause")
    {
        return "PauseDone";
    }
    else if(buffer == "Resume")
    {
        return "ResumeDone";
    }
    else if(buffer.substr(0, 9) == "Configure")
    {
        return "ConfigureDone";
    }

    else if(buffer.substr(0, 5) == "K2410")
    {
        std::cout << "Special handling of K2410" << std::endl;
        std::string command = getVariableValue("K2410", buffer);

        std::string powerSupplyId = getVariableValue("PowerSupplyId", buffer);
        std::string channelId     = getVariableValue("ChannelId", buffer);
        auto        channelK2410  = static_cast<KeithleyChannel*>(fHandler.getPowerSupply(powerSupplyId)->getChannel(channelId));

        if(command == "setupIsense")
        {
            float currentCompliance = std::stof(getVariableValue("CurrCompl", buffer));

            channelK2410->setVoltageMode();
            channelK2410->setVoltage(0.0);
            channelK2410->setCurrentCompliance(currentCompliance);
        }
        else if(command == "setupVsense")
        {
            float voltageCompliance = std::stof(getVariableValue("VoltCompl", buffer));

            channelK2410->setCurrentMode();
            channelK2410->setCurrent(0.0);
            channelK2410->setParameter("Isrc_range", (float)1e-6);
            channelK2410->setVoltageCompliance(voltageCompliance);
        }
        else if(command == "setupIsource")
        {
            float current           = std::stof(getVariableValue("Current", buffer));
            float voltageCompliance = std::stof(getVariableValue("VoltCompl", buffer));

            channelK2410->setCurrentMode();
            channelK2410->setCurrent(current);
            channelK2410->setVoltageCompliance(voltageCompliance);
        }
        else if(command == "setupVsource")
        {
            float voltage           = std::stof(getVariableValue("Voltage", buffer));
            float currentCompliance = std::stof(getVariableValue("CurrCompl", buffer));

            channelK2410->setVoltageMode();
            channelK2410->setVoltage(voltage);
            channelK2410->setCurrentCompliance(currentCompliance);
        }
        else if(command == "SetVoltage")
        {
            std::string powerSupplyId = getVariableValue("PowerSupplyId", buffer);
            std::string channelId     = getVariableValue("ChannelId", buffer);
            float       voltage       = std::stof(getVariableValue(",Voltage", buffer)); // "," important otherwise will find "SetVoltage" as variable

            std::cout << "Setting voltage to " << std::to_string(voltage) << " V for Power supply = " << powerSupplyId << " ChannelId = " << channelId << std::endl;

            channelK2410->setParameter("Vsrc_range", getVoltageRange(voltage));
            channelK2410->setVoltage(voltage);
        }
        else
        {
            std::cerr << __PRETTY_FUNCTION__ << " Couldn't recognige K2410 command: " << buffer << ". Aborting..." << std::endl;
            return "Didn't understand the K2410 command!";
        }

        return "K2410 done";
    }
    else if(valuesMap["Header"] == "GetDeviceConnected")
    {
        bool verbose = false;
        auto it      = valuesMap.find("verbose");
        if(it != valuesMap.end()) std::istringstream(it->second) >> std::boolalpha >> verbose;
        std::string replayMessage;
        replayMessage += "TimeStamp:" + getTimeStamp();
        replayMessage += ",ChannelList:{";
        for(const auto& readoutChannel: fHandler.getReadoutList()) { replayMessage += readoutChannel + ","; }
        replayMessage.erase(replayMessage.size() - 1);
        replayMessage += "}";
        return replayMessage;
    }
    else if(valuesMap["Header"] == "TurnOn")
    {
        std::string powerSupplyId = valuesMap["DeviceId"];
        std::string channelId     = valuesMap["ChannelId"];
        std::cout << fHandler.getPowerSupply(powerSupplyId) << std::endl;
        auto channel = fHandler.getPowerSupply(powerSupplyId)->getChannel(channelId);
        channel->turnOn();
        std::cout << "Power supply = " << powerSupplyId << " ChannelId = " << channelId << " is On : " << channel->isOn() << std::endl;
        return "TurnOnDone";
    }
    else if(valuesMap["Header"] == "TurnOff")
    {
        std::string powerSupplyId = valuesMap["DeviceId"];
        std::string channelId     = valuesMap["ChannelId"];
        bool        verbose       = false;
        auto        it            = valuesMap.find("verbose");
        if(it != valuesMap.end()) std::istringstream(it->second) >> std::boolalpha >> verbose;
        auto channel = fHandler.getPowerSupply(powerSupplyId)->getChannel(channelId);
        channel->turnOff();
        std::cout << "Power supply = " << powerSupplyId << " ChannelId = " << channelId << " is On : " << channel->isOn() << std::endl;
        return "TurnOffDone";
    }
    else if(valuesMap["Header"] == "GetStatus")
    {
        bool verbose = false;
        auto it      = valuesMap.find("verbose");
        if(it != valuesMap.end()) std::istringstream(it->second) >> std::boolalpha >> verbose;
        std::string replayMessage;
        replayMessage += "TimeStamp:" + getTimeStamp();
        for(const auto& readoutChannel: fHandler.getStatus()) { replayMessage += ("," + readoutChannel); }
        return replayMessage;
    }
    else if(valuesMap["Header"] == "SetVoltage")
    {
        std::string powerSupplyId = valuesMap["DeviceId"];
        std::string channelId     = valuesMap["ChannelId"];
        float       voltage       = std::stof(valuesMap["Voltage"]); // "," important otherwise will find "SetVoltage" as variable
        bool        verbose       = false;
        auto        it            = valuesMap.find("verbose");
        if(it != valuesMap.end()) std::istringstream(it->second) >> std::boolalpha >> verbose;
        std::cout << "Setting voltage to " << std::to_string(voltage) << " V for Power supply = " << powerSupplyId << " ChannelId = " << channelId << std::endl;

        auto channel = fHandler.getPowerSupply(powerSupplyId)->getChannel(channelId);
        channel->setVoltage(voltage);
        //fHandler.getPowerSupply(powerSupplyId)->getChannel(channelId)->setVoltage(voltage);
        
	return "SetVoltageDone";
    }
    else if(valuesMap["Header"] == "GetVoltage") //(buffer.substr(0, 10) == "GetVoltage")
    {
        std::string deviceId  = valuesMap["DeviceId"];
        std::string channelId = valuesMap["ChannelId"];
        bool        verbose   = false;
        auto        it        = valuesMap.find("verbose");
        if(it != valuesMap.end()) std::istringstream(it->second) >> std::boolalpha >> verbose;
        try
        {
            float voltage = fHandler.getDevice(deviceId, channelId)->vRead();
            std::cout << "Got voltage = " << std::to_string(voltage) << " V from Device = " << deviceId << " ChannelId = " << channelId << std::endl;
            return std::to_string(voltage);
        }
        catch(const std::out_of_range& oor)
        {
            std::cerr << oor.what() << std::endl;
        }
        catch(const std::runtime_error& re)
        {
            std::cerr << re.what() << std::endl;
        }
    }
    else if(valuesMap["Header"] == "SetCurrent")
    {
        std::string powerSupplyId = valuesMap["DeviceId"];
        std::string channelId     = valuesMap["ChannelId"];
        float       current       = std::stof(valuesMap["Current"]); // "," important otherwise will find "SetVoltage" as variable

        std::cout << "Setting current to " << std::to_string(current) << " A for Power supply = " << powerSupplyId << " ChannelId = " << channelId << std::endl;

        auto channel = fHandler.getPowerSupply(powerSupplyId)->getChannel(channelId);
        channel->setCurrent(current);

        return "SetCurrentDone";
    }
    else if(valuesMap["Header"] == "GetCurrent") // (buffer.substr(0, 10) == "GetCurrent")
    {
        std::string deviceId  = valuesMap["DeviceId"];
        std::string channelId = valuesMap["ChannelId"];
        bool        verbose   = false;
        auto        it        = valuesMap.find("verbose");
        if(it != valuesMap.end()) std::istringstream(it->second) >> std::boolalpha >> verbose;
        try
        {
            float current = fHandler.getDevice(deviceId, channelId)->iRead();
            std::cout << "Got current = " << std::to_string(current) << " A from Device = " << deviceId << " ChannelId = " << channelId << std::endl;
            return std::to_string(current);
        }
        catch(const std::out_of_range& oor)
        {
            std::cerr << oor.what() << std::endl;
        }
        catch(const std::runtime_error& re)
        {
            std::cerr << re.what() << std::endl;
        }
    }
    else if(valuesMap["Header"] == "GetResistance")
    {
        std::string deviceId  = valuesMap["DeviceId"];
        std::string channelId = valuesMap["ChannelId"];
        bool        verbose   = false;
        auto        it        = valuesMap.find("verbose");
        if(it != valuesMap.end()) std::istringstream(it->second) >> std::boolalpha >> verbose;
        try
        {
            float resistance = fHandler.getDevice(deviceId, channelId)->rRead();
            std::cout << "Got Resistance = " << std::to_string(resistance) << "Ohm from Device = " << deviceId << " ChannelId = " << channelId << std::endl;
            return std::to_string(resistance);
        }
        catch(const std::out_of_range& oor)
        {
            std::cerr << oor.what() << std::endl;
        }
        catch(const std::runtime_error& re)
        {
            std::cerr << re.what() << std::endl;
        }
    }
    else if(valuesMap["Header"] == "CanGetVoltage")
    {
        std::string deviceId  = valuesMap["DeviceId"];
        std::string channelId = valuesMap["ChannelId"];
        bool        verbose   = false;
        auto        it        = valuesMap.find("verbose");
        if(it != valuesMap.end()) std::istringstream(it->second) >> std::boolalpha >> verbose;
        try
        {
            bool        voltageQ = fHandler.getDevice(deviceId, channelId)->vReadQ();
            std::string vAnswer  = (voltageQ ? "True" : "False");
            std::cout << "Can Device = " << deviceId << " ChannelId = " << channelId << " read Voltage? " << vAnswer << std::endl;
            return (vAnswer);
        }
        catch(const std::out_of_range& oor)
        {
            std::cerr << oor.what() << std::endl;
        }
        catch(const std::runtime_error& re)
        {
            std::cerr << re.what() << std::endl;
        }
    }
    else if(valuesMap["Header"] == "CanGetCurrent")
    {
        std::string deviceId  = valuesMap["DeviceId"];
        std::string channelId = valuesMap["ChannelId"];
        bool        verbose   = false;
        auto        it        = valuesMap.find("verbose");
        if(it != valuesMap.end()) std::istringstream(it->second) >> std::boolalpha >> verbose;
        try
        {
            bool        currentQ = fHandler.getDevice(deviceId, channelId)->iReadQ();
            std::string iAnswer  = (currentQ ? "True" : "False");
            std::cout << "Can Device = " << deviceId << " ChannelId = " << channelId << " read Current? " << iAnswer << std::endl;
            return (iAnswer);
        }
        catch(const std::out_of_range& oor)
        {
            std::cerr << oor.what() << std::endl;
        }
        catch(const std::runtime_error& re)
        {
            std::cerr << re.what() << std::endl;
        }
    }
    else if(valuesMap["Header"] == "CanGetResistance")
    {
        std::string deviceId  = valuesMap["DeviceId"];
        std::string channelId = valuesMap["ChannelId"];
        if(channelId.empty()) channelId = "(Not Defined)";
        bool verbose = false;
        auto it      = valuesMap.find("verbose");
        if(it != valuesMap.end()) std::istringstream(it->second) >> std::boolalpha >> verbose;
        try
        {
            bool        resistanceQ = fHandler.getDevice(deviceId, channelId)->rReadQ();
            std::string rAnswer     = (resistanceQ ? "True" : "False");
            std::cout << "Can Device = " << deviceId << " ChannelId = " << channelId << " read Resistance? " << rAnswer << std::endl;
            return (rAnswer);
        }
        catch(const std::out_of_range& oor)
        {
            std::cerr << oor.what() << std::endl;
        }
        catch(const std::runtime_error& re)
        {
            std::cerr << re.what() << std::endl;
        }
    }
    else if(valuesMap["Header"] == "RunITIVSLDOScan") // IV curve testing message
    {
        const std::string configFile = valuesMap["configFile"];
        std::string       cmdLine    = "ITIVCurve -s -c " + configFile;
        bool              verbose    = convertCommandValueToBool(valuesMap, "verbose");
        if(verbose) cmdLine += " -v";
        std::cout << "Executing the following command line: " << cmdLine << std::endl;
        int v = system(cmdLine.c_str());
        std::cout << "Execution status is: " << v << std::endl;
        return "IVITSLDOScanDone";
    }
    else if(valuesMap["Header"] == "InitITIVTools")
    {
        const std::string configFile = valuesMap["configFile"];
        bool              verbose    = convertCommandValueToBool(valuesMap, "verbose");
        verbose                      = true;
        fITIVToolTest                = new ITIVTools(configFile, true, verbose); // Hardcoded true means you don't want to switch off PS after data acquisition
        fITIVToolTest->ConfigureSaving();
        fITIVToolTest->PrepareFileHeaderScanner();
        fITIVToolTest->GetSaveFileDescriptor()->close();
        return "InitITIVToolsDone";
    }
    else if(valuesMap["Header"] == "PrepareMultimeter")
    {
        const std::string multimeterId = valuesMap["multimeterId"];
        auto              multimeter   = static_cast<KeithleyMultimeter*>(fHandler.getMultimeter(multimeterId));
        multimeter->reset();
        multimeter->enableInternalScan();
        return "PrepareMultimeterDone";
    }
    else if(valuesMap["Header"] == "ReadScannerCardPoint")
    {
        // const std::regex  e(";");
        const std::string multimeterId = valuesMap["multimeterId"];
        std::string       psRead       = valuesMap["psRead"];
        // psRead                                  = std::regex_replace(psRead, e, ",");
        std::replace(psRead.begin(), psRead.end(), ';', ',');

        std::map<int, std::string> channelMap   = fITIVToolTest->GetScannerCardMap();
        const std::string          saveFileName = fITIVToolTest->GetSaveCompleteFileName();
        std::ofstream*             saveFile     = new std::ofstream;
        auto                       multimeter   = static_cast<KeithleyMultimeter*>(fHandler.getMultimeter(multimeterId));
        saveFile->open(saveFileName, std::ofstream::out | std::ofstream::app);
        std::cout << "psRead: " << psRead << std::endl;
        std::cout << "First channel: " << channelMap.begin()->first << std::endl;
        std::string readStr = multimeter->scanChannels(channelMap.begin()->first, channelMap.end()->first);
        if(readStr.back() != '\n')
        {
            std::cout << "Entering adding newline" << std::endl;
            readStr.push_back('\n');
        }
        *saveFile << psRead;
        *saveFile << readStr;
        saveFile->close();
        return "ReadScannerCardPointDone";
    }
    else if(valuesMap["Header"] == "RunAnalysis")
    {
        fITIVToolTest->CreateLastScanFile();
        fITIVToolTest->RunAnalysis();
        return "RunAnalysisDone";
    }
    else if(valuesMap["Header"] == "EndAcquisition")
    {
        fITIVToolTest->EndAcquisition();
        return "EndAcquisitionDone";
    }
    else if(valuesMap["Header"] == "ImuxFileHeader")
    {
        // const std::regex e(";");
        std::string header = valuesMap["FileHeader"];
        // header                      = std::regex_replace(header, e, ",");
        std::replace(header.begin(), header.end(), ';', ',');

        std::string saveDir         = fITIVToolTest->GetSaveDir();
        std::string saveFileName    = fITIVToolTest->GetSaveFileName();
        std::string saveFileNameMux = saveDir + "imux_" + saveFileName;
        std::cout << "saveFileNameMux: " << saveFileNameMux << std::endl;
        fITIVToolTest->SetSaveFileNameMux(saveFileNameMux);

        std::ofstream* saveFileMux = new std::ofstream;
        saveFileMux->open(saveFileNameMux, std::ofstream::out | std::ofstream::app);
        *saveFileMux << header;
        saveFileMux->close();
        return "ImuxFileHeaderPrepared";
    }
    else if(valuesMap["Header"] == "ImuxReading")
    {
        // const std::regex e(";");
        std::string imuxValues = valuesMap["Reading"];
        // imuxValues                  = std::regex_replace(imuxValues, e, ",");
        std::replace(imuxValues.begin(), imuxValues.end(), ';', ',');

        std::string saveFileNameMux = fITIVToolTest->GetSaveFileNameMux();
        std::cout << "saveFileNameMux: " << saveFileNameMux << std::endl;
        std::ofstream* saveFileMux = new std::ofstream;
        saveFileMux->open(saveFileNameMux, std::ofstream::out | std::ofstream::app);
        *saveFileMux << std::endl;
        *saveFileMux << imuxValues;
        saveFileMux->close();
        return "ImuxFileWritten";
    }
    else if(buffer.substr(0, 14) == "SetVProtection")
    {
        std::string powerSupplyId = getVariableValue("PowerSupplyId", buffer);
        std::string channelId     = getVariableValue("ChannelId", buffer);
        float       protection    = std::stof(getVariableValue("VoltageProtection", buffer));
        auto        channel       = fHandler.getPowerSupply(powerSupplyId)->getChannel(channelId);
        channel->setOverVoltageProtection(protection);
        std::cout << "Power supply = " << powerSupplyId << " ChannelId = " << channelId << " overvoltage protection set to : " << channel->getOverVoltageProtection() << std::endl;
        return "SetOverVoltageProtectionDone";
    }
    else if(buffer.substr(0, 14) == "SetVCompliance")
    {
        std::string powerSupplyId = getVariableValue("PowerSupplyId", buffer);
        std::string channelId     = getVariableValue("ChannelId", buffer);
        float       compliance    = std::stof(getVariableValue("VoltageCompliance", buffer));
        auto        channel       = fHandler.getPowerSupply(powerSupplyId)->getChannel(channelId);
        channel->setVoltageCompliance(compliance);
        std::cout << "Power supply = " << powerSupplyId << " ChannelId = " << channelId << " compliance set to : " << channel->getVoltageCompliance() << std::endl;
        return "SetVoltageComplianceDone";
    }
    else if(buffer.substr(0, 6) == "Error:")
    {
        if(buffer == "Error: Connection closed") std::cerr << __PRETTY_FUNCTION__ << buffer << ". Closing client server connection!" << std::endl;
        return "";
    }
    else
    {
        std::cerr << __PRETTY_FUNCTION__ << " Can't recognize message: " << buffer << ". Aborting..." << std::endl;
        abort();
    }

    if(running_ || paused_) // We go through here after start and resume or
                            // pause: sending back current status
    { std::cout << "Getting time and status here" << std::endl; }

    return "Didn't understand the message!";
}

float ITPowerSupplyInterface::getVoltageRange(float voltage)
{
    if(voltage < (float)0.2)
        return 0.2;
    else if(voltage < 2)
        return 2;
    else if(voltage < 20)
        return 20;
    else
        return 1000;
}

//========================================================================================================================
std::string ITPowerSupplyInterface::getTimeStamp()
{
    time_t     rawtime;
    struct tm* timeinfo;
    char       buffer[80];

    time(&rawtime);
    timeinfo = localtime(&rawtime);

    strftime(buffer, sizeof(buffer), "%Y-%m-%d %H:%M:%S", timeinfo);
    std::string str(buffer);
    return str;
}

// Antonio
/*!
************************************************
 * Utility to split strings based on given
 * separator.
 \param line String to be splitted.
 \param separator Separator character used to
 split strings (default is ',')
 \return Returns a vector with splitted strings.
************************************************
*/
std::vector<std::string> ITPowerSupplyInterface::splitString(const std::string& line, const char& separator)
{
    std::stringstream        buffer(line);
    std::string              aux;
    std::vector<std::string> splittedString;

    while(std::getline(buffer, aux, separator)) splittedString.push_back(aux);
    return splittedString;
}

/*!
************************************************
 * Creates a variable/value map starting from
 * command string and based on a given
 * separator.
 \param line Command input string.
 \param separator Separator used to create
 the map (default value is ':')
 \return Returns a map with variable
 name/variable value.
************************************************
*/
std::map<std::string, std::string> ITPowerSupplyInterface::createVariablesMap(const std::string& line, const char& separator)
{
    std::map<std::string, std::string> variablesMap;
    const std::vector<std::string>     splittedCommandString = splitString(line);
    variablesMap["Header"]                                   = splittedCommandString[0];
    for(unsigned int v = 1; v < splittedCommandString.size(); ++v)
    {
        std::vector<std::string> splitCommand = splitString(splittedCommandString[v], separator);
        if(splitCommand.size() != 2)
        {
            std::stringstream error;
            error << "Bad command variable value formatting for \"" << splittedCommandString[v] << "\" in command line: \"" << line << "\"";
            throw std::runtime_error(error.str());
        }
        const std::string key = eraseFirstBlankCharacter(splitCommand[0]);
        const std::string val = eraseFirstBlankCharacter(splitCommand[1]);
        variablesMap[key]     = val;
    }
    return variablesMap;
}

/*!
************************************************
 * Strips first char from a string if blank.
 \param str String to be transformed.
 \return Transformed string.
************************************************
*/
std::string ITPowerSupplyInterface::eraseFirstBlankCharacter(const std::string& str)
{
    if(str.substr(0, 1) == " ")
        return str.substr(1, str.size());
    else
        return str;
}

/*!
************************************************
 * Converts value in command map to bool.
 \param valuesMap Map search.
 \param str Bool value name.
 \return boolean true or false.
************************************************
*/
bool ITPowerSupplyInterface::convertCommandValueToBool(const std::map<std::string, std::string>& valuesMap, const std::string& str)
{
    bool boolVal = false;
    auto it      = valuesMap.find(str);
    if(it != valuesMap.end()) std::istringstream(it->second) >> std::boolalpha >> boolVal;
    return boolVal;
}
