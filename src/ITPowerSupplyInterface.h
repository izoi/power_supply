#ifndef _ITPowerSupplyInterface_h_
#define _ITPowerSupplyInterface_h_

#include "../NetworkUtils/TCPServer.h"
#include "DeviceHandler.h"
#include "ITIVTools.h"
#include "Keithley.h"
#include "KeithleyMultimeter.h"
#include "PowerSupply.h"
#include "PowerSupplyChannel.h"

#include <mutex>
#include <sstream>
#include <string>
#include <thread>
#include <vector>

// Antonio
//#include <boost/algorithm/string.hpp>
//#include <boost/filesystem.hpp>
//#include <cmath>
//#include <cstdlib>
//#include <errno.h>
//#include <fstream>
//#include <iomanip>
//#include <iostream>
//#include <limits>
//#include <pugixml.hpp>
//#include <regex>

class ITPowerSupplyInterface : public TCPServer
{
  public:
    ITPowerSupplyInterface(int serverPort, std::string configFileName);
    ITPowerSupplyInterface(int serverPort);
    virtual ~ITPowerSupplyInterface(void);

    std::string interpretMessage(const std::string& buffer) override;

  private:
    std::vector<std::string>           splitString(const std::string& line, const char& separator = ',');
    std::string                        eraseFirstBlankCharacter(const std::string& str);
    bool                               convertCommandValueToBool(const std::map<std::string, std::string>& valuesMap, const std::string& str);
    std::map<std::string, std::string> createVariablesMap(const std::string& line, const char& separator = ':');
    std::string                        getVariableValue(std::string variable, std::string buffer)
    {
        size_t begin = buffer.find(variable) + variable.size() + 1;
        size_t end   = buffer.find(',', begin);
        if(end == std::string::npos) end = buffer.size();
        return buffer.substr(begin, end - begin);
    }
    std::string        currentRun_ = "0";
    bool               running_    = false;
    bool               paused_     = false;
    DeviceHandler      fHandler;
    pugi::xml_document fDocSettings;

    float       getVoltageRange(float voltage);
    std::string getTimeStamp();

    std::mutex fMutex;

    // Scanner card Antonio
    ITIVTools* fITIVToolTest;
};

#endif
