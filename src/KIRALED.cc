#include "KIRALED.h"
#include <iostream>
#include <unistd.h>

KIRALED::KIRALED(Connection* connection, const pugi::xml_node configuration) : fConfiguration(configuration), fId(configuration.attribute("ID").value()), fConnection(connection) { configure(); }
KIRALED::~KIRALED() {}

void KIRALED::configure()
{
    fChannel = fConfiguration.attribute("Channel").as_int();
    fSlot    = fConfiguration.attribute("Slot").as_string();

    fIndex = std::to_string(fChannel);
    if(fIndex.length() == 1) { fIndex = "0" + fIndex; }
    fIsOn = std::string(fConfiguration.attribute("Light").value()).compare("on") == 0;
    setIntensity(fConfiguration.attribute("Intensity").as_int());
}

void KIRALED::setIntensity(uint32_t pIntensity)
{
    // Check if the LED is currently on, if yes change emiited instensity, if not only change the value in the memory
    fIntensity = pIntensity;
    std::cout << "set intensity to " << fIntensity << std::endl;
    if(fIsOn) { turnOn(); }
    else
    {
        turnOff();
    }
}

void KIRALED::turnOn(void)
{
    fIsOn = true;
    std::cout << "turn on LED " << fId << "\tindex:\t" << fIndex << std::endl;
    std::cout << "intensity_" + fIndex + "_" + std::to_string(fIntensity) << std::endl;
    fConnection->write("intensity_" + fIndex + "_" + std::to_string(fIntensity)); // Set intensity to previous value to turn on
}

void KIRALED::turnOff(void)
{
    fIsOn = false;
    std::cout << "turn off LED " << fId << std::endl;
    std::cout << "intensity_" + fIndex + "_0" << std::endl;
    fConnection->write("intensity_" + fIndex + "_0"); // Set intensity to zero to shut LED off
}
bool KIRALED::isOn(void)
{
    return fIsOn; // If intensity is larger than 0 the LED is supposed to be "on"
}