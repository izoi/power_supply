/*!
 * \authors Stefan Maier <s.maier@kit.edu>, KIT
 * \date Sep 2 2019
 */

#ifndef KIRALED_H
#define KIRALED_H
#include <cstdint>
#include "SerialConnection.h"
#include "pugixml.hpp"

/*!
************************************************
 \class KIRALED.
 \brief Abstract LED Channel class for led management.
************************************************
*/

class KIRALED
{
  public:
    KIRALED(Connection* connection, const pugi::xml_node configuration);
    ~KIRALED();

    void configure(void);

    std::string getID() const { return fId; }

    void turnOn(void);
    void turnOff(void);
    bool isOn(void);

    // Get/set methods
    void     setIntensity(uint32_t pIntensity);
    uint32_t getIntensity(void);

  protected:
    const pugi::xml_node fConfiguration;
    std::string          fId;
    Connection*          fConnection;

  private:
    uint32_t    fIntensity = 0;
    uint8_t     fChannel;
    std::string fSlot;
    std::string fIndex;

    bool fIsOn = false;
};

#endif
