#include <boost/algorithm/string.hpp>
#include <ctime>
#include <iostream>
#include <stdio.h>
#include <unistd.h>

#include "PowerSupplyInterface.h"

//========================================================================================================================
PowerSupplyInterface::PowerSupplyInterface(int serverPort, std::string configFileName) : TCPServer(serverPort, 10) { fHandler.readSettings(configFileName, fDocSettings); }

PowerSupplyInterface::PowerSupplyInterface(int serverPort) : TCPServer(serverPort, 10) { std::cout << "Ready to receive data!" << std::endl; }
//========================================================================================================================
PowerSupplyInterface::~PowerSupplyInterface(void) { std::cout << __PRETTY_FUNCTION__ << " DESTRUCTOR" << std::endl; }

//========================================================================================================================
std::string PowerSupplyInterface::interpretMessage(const std::string& buffer)
{
    std::lock_guard<std::mutex> theGuard(fMutex);

    std::cout << __PRETTY_FUNCTION__ << " Message received " << buffer << std::endl;

    std::map<std::string, std::string> valuesMap;
    try
    {
        valuesMap = createVariablesMap(buffer);
    }
    catch(const std::runtime_error& rte)
    {
        std::cout << "Entering error" << std::endl;
        std::cerr << rte.what() << '\n';
        throw std::runtime_error(rte.what());
    }
    if(buffer == "Initialize") // Changing the status changes the mode in
                               // threadMain (BBC) function
    { return "InitializeDone"; }
    else if(valuesMap["Header"] == "Start") // Changing the status changes the                           // buffer.substr(0, 5)
                                            // mode in threadMain (BBC)
                                            // function
    {
        return "StartDone";
    }
    else if(valuesMap["Header"] == "Stop") // buffer.substr(0, 4)
    {
        return "StopDone";
    }
    else if(valuesMap["Header"] == "Halt") // buffer.substr(0, 4)
    {
        return "HaltDone";
    }
    else if(buffer == "Pause")
    {
        return "PauseDone";
    }
    else if(buffer == "Resume")
    {
        return "ResumeDone";
    }
    else if(valuesMap["Header"] == "Configure") // buffer.substr(0, 9)
    {
        return "ConfigureDone";
    }
    else if(valuesMap["Header"] == "GetDeviceConnected")
    {
        std::string replayMessage;
        replayMessage += "TimeStamp:" + getTimeStamp();
        replayMessage += ",ChannelList:{";
        for(const auto& readoutChannel: fHandler.getReadoutList()) { replayMessage += readoutChannel + ","; }
        replayMessage.erase(replayMessage.size() - 1);
        replayMessage += "}";
        return replayMessage;
    }
    else if(valuesMap["Header"] == "TurnOn")
    {
        std::string powerSupplyId = valuesMap["PowerSupplyId"];
        std::string channelId     = valuesMap["ChannelId"];
        auto        channel       = fHandler.getPowerSupply(powerSupplyId)->getChannel(channelId);
        channel->turnOn();
        std::cout << "Power supply = " << powerSupplyId << " ChannelId = " << channelId << " is On : " << channel->isOn() << std::endl;
        return "TurnOnDone";
    }
    else if(valuesMap["Header"] == "TurnOff")
    {
        std::string powerSupplyId = valuesMap["PowerSupplyId"];
        std::string channelId     = valuesMap["ChannelId"];
        auto        channel       = fHandler.getPowerSupply(powerSupplyId)->getChannel(channelId);
        channel->turnOff();
        std::cout << "Power supply = " << powerSupplyId << " ChannelId = " << channelId << " is On : " << channel->isOn() << std::endl;
        return "TurnOffDone";
    }
    else if(valuesMap["Header"] == "GetStatus")
    {
        std::string replayMessage;
        replayMessage += "TimeStamp:" + getTimeStamp();
        std::string powerSupplyId = valuesMap["PowerSupplyId"];
        if(powerSupplyId != "")
        {
            for(const auto& readoutChannel: fHandler.getStatus(powerSupplyId)) { replayMessage += ("," + readoutChannel); }
        }
        else
        {
            for(const auto& readoutChannel: fHandler.getStatus()) { replayMessage += ("," + readoutChannel); }
        }
        std::cout << "Message: \t " << replayMessage << std::endl;

        return replayMessage;
    }
    else if(valuesMap["Header"] == "SetVoltage")
    {
        std::string powerSupplyId = valuesMap["PowerSupplyId"];
        std::string channelId     = valuesMap["ChannelId"];
        float       voltage       = std::stof(valuesMap["Voltage"]); // "," important otherwise will find "SetVoltage" as variable
        std::cout << "Setting voltage to " << std::to_string(voltage) << " V for Power supply = " << powerSupplyId << " ChannelId = " << channelId << std::endl;

        fHandler.getPowerSupply(powerSupplyId)->getChannel(channelId)->setVoltage(voltage);
        auto channel = fHandler.getPowerSupply(powerSupplyId)->getChannel(channelId);
        try
        {
            float protection = channel->getOverVoltageProtection();
            if(voltage > protection) std::cout << " WARNING: Voltage is higher than OverVoltage Protection. " << std::endl;
        }
        catch(std::runtime_error& re)
        {
            std::cout << "No OverVoltage protection possible with this power supply" << std::endl;
        }
        return "SetVoltageDone";
    }
    else if(valuesMap["Header"] == "GetVoltage") //(buffer.substr(0, 10) == "GetVoltage")
    {
        std::string powerSupplyId = valuesMap["PowerSupplyId"];
        std::string channelId     = valuesMap["ChannelId"];
        float       voltage       = fHandler.getPowerSupply(powerSupplyId)->getChannel(channelId)->getOutputVoltage();

        std::cout << "Got voltage = " << std::to_string(voltage) << " V from Power supply = " << powerSupplyId << " ChannelId = " << channelId << std::endl;
        return std::to_string(voltage);
    }
    else if(valuesMap["Header"] == "SetCurrent")
    {
        std::string powerSupplyId = valuesMap["PowerSupplyId"];
        std::string channelId     = valuesMap["ChannelId"];
        float       current       = std::stof(valuesMap["Current"]); // "," important otherwise will find "SetVoltage" as variable

        fHandler.getPowerSupply(powerSupplyId)->getChannel(channelId)->setCurrent(current);
        std::cout << "Setting current to " << std::to_string(current) << " A for Power supply = " << powerSupplyId << " ChannelId = " << channelId << std::endl;
        return "SetCurrentDone";
    }
    else if(valuesMap["Header"] == "GetCurrent") // (buffer.substr(0, 10) == "GetCurrent")
    {
        std::string powerSupplyId = valuesMap["PowerSupplyId"];
        std::string channelId     = valuesMap["ChannelId"];
        float       current       = fHandler.getPowerSupply(powerSupplyId)->getChannel(channelId)->getCurrent();
        std::cout << "Got current = " << std::to_string(current) << " A from Power supply = " << powerSupplyId << " ChannelId = " << channelId << std::endl;
        return std::to_string(current);
    }
    else if(valuesMap["Header"] == "SetVProtection")
    {
        std::string powerSupplyId = valuesMap["PowerSupplyId"];
        std::string channelId     = valuesMap["ChannelId"];
        float       protection    = std::stof(valuesMap["VoltageProtection"]);
        auto        channel       = fHandler.getPowerSupply(powerSupplyId)->getChannel(channelId);
        channel->setOverVoltageProtection(protection);
        std::cout << "Power supply = " << powerSupplyId << " ChannelId = " << channelId << " overvoltage protection set to : " << channel->getOverVoltageProtection() << std::endl;
        return "SetOverVoltageProtectionDone";
    }
    else if(valuesMap["Header"] == "SetVCompliance")
    {
        std::string powerSupplyId = valuesMap["PowerSupplyId"];
        std::string channelId     = valuesMap["ChannelId"];
        float       compliance    = std::stof(valuesMap["VoltageCompliance"]);
        auto        channel       = fHandler.getPowerSupply(powerSupplyId)->getChannel(channelId);
        channel->setVoltageCompliance(compliance);
        std::cout << "Power supply = " << powerSupplyId << " ChannelId = " << channelId << " compliance set to : " << channel->getVoltageCompliance() << std::endl;
        return "SetVoltageComplianceDone";
    }
    else if(buffer.substr(0, 6) == "Error:")
    {
        if(buffer == "Error: Connection closed") std::cerr << __PRETTY_FUNCTION__ << buffer << ". Closing client server connection!" << std::endl;
        return "";
    }
    else if(valuesMap["Header"] == "Scope:")
    {
        std::vector<std::string> arguments;
        boost::split(arguments, buffer, boost::is_any_of(":"));
        std::string          channel      = arguments.at(1);
        std::string          command      = arguments.at(2);
        ScopeAgilent*        scope        = dynamic_cast<ScopeAgilent*>(fHandler.getScope("AgilentScope"));
        ScopeAgilentChannel* scopechannel = dynamic_cast<ScopeAgilentChannel*>(scope->getChannel(channel));
        if(command.substr(0, 7) == "setEOM=")
        {
            std::vector<std::string> subargs;
            boost::split(subargs, command, boost::is_any_of("="));
            scopechannel->setEOMeasurement(subargs.at(1));
            return "Observables for EOM set.";
        }
        if(command.substr(0, 11) == "acquireEOM=")
        {
            std::vector<std::string> subargs;
            boost::split(subargs, command, boost::is_any_of("="));
            return scopechannel->acquireEOM(1000, 20, 80, subargs.at(1));
        }
    }
    else if(valuesMap["Header"] == "GetArduinoStatus")
    {
        std::string replayMessage;
        replayMessage += "TimeStamp:" + getTimeStamp();
        std::string arduinoId = valuesMap["ArduinoId"];
        if(arduinoId != "")
        {
            for(const auto& readoutChannel: fHandler.getArduinoStatus(arduinoId)) { replayMessage += ("," + readoutChannel); }
        }
        else
        {
            for(const auto& readoutChannel: fHandler.getArduinoStatus()) { replayMessage += ("," + readoutChannel); }
        }
        std::cout << "Message: \t " << replayMessage << std::endl;
        return replayMessage;
    }
    else if(valuesMap["Header"] == "GetKIRAFWVersion")
    {
        std::string replayMessage;
        replayMessage += "TimeStamp:" + getTimeStamp();
        std::string arduinoId = valuesMap["ArduinoId"];
        std::string version   = fHandler.getArduino(arduinoId)->getParameterString("version");

        std::cout << "Message: \t " << replayMessage << std::endl;
        replayMessage += "," + arduinoId + "_" + arduinoId + "_Version:" + version;
        return replayMessage;
    }
    else if(valuesMap["Header"] == "KIRATriggerFrequency") // KIRATriggerFrequency,ArduinoId:abc,TriggerFrequency:123
    {
        std::string arduinoId = valuesMap["ArduinoId"];
        uint32_t    frequency = std::stoi(valuesMap["TriggerFrequency"]);
        fHandler.getArduino(arduinoId)->setParameter("frequency", (int)frequency);
        return "KIRAFrequency Set To " + valuesMap["TriggerFrequency"];
    }
    else if(valuesMap["Header"] == "KIRATrigger") // KIRATrigger,ArduinoId:abc,Trigger:on/off
    {
        std::string arduinoId = valuesMap["ArduinoId"];
        std::string value     = valuesMap["Trigger"];
        fHandler.getArduino(arduinoId)->setParameter("trigger", value);
        return "KIRATrigger Set " + valuesMap["Trigger"];
    }
    else if(valuesMap["Header"] == "KIRAPulseLength") // KIRAPulseLength,ArduinoId:abc,PulseLength:123
    {
        std::string arduinoId = valuesMap["ArduinoId"];
        uint32_t    length    = std::stoi(valuesMap["PulseLength"]);
        fHandler.getArduino(arduinoId)->setParameter("pulseLength", (int)length);
        return "KIRAPulseLength Set To " + valuesMap["PulseLength"];
    }
    else if(valuesMap["Header"] == "KIRADacLed") // KIRADacLed,ArduinoId:abc,DacLed:high/low
    {
        std::string arduinoId = valuesMap["ArduinoId"];
        std::string value     = valuesMap["DacLed"];
        fHandler.getArduino(arduinoId)->setParameter("dacLed", value);
        return "KIRADacLed Set To " + valuesMap["DacLed"];
    }
    else if(valuesMap["Header"] == "KIRALight") // KIRALight,ArduinoId:abc,LED:led1,Light:on/off
    {
        std::string arduinoId = valuesMap["ArduinoId"];
        std::string led       = valuesMap["LED"];
        std::string value     = valuesMap["Light"];
        fHandler.getArduino(arduinoId)->setParameter("led", value, led);
        return "KIRALed " + valuesMap["LED"] + " Set " + valuesMap["Light"];
    }
    else if(valuesMap["Header"] == "KIRAIntensity") // KIRAIntensity,ArduinoId:abc,LED:led1,Intensity:123
    {
        std::string arduinoId = valuesMap["ArduinoId"];
        std::string led       = valuesMap["LED"];
        uint32_t    intensity = std::stoi(valuesMap["Intensity"]);
        fHandler.getArduino(arduinoId)->setParameter("intensity", (int)intensity, led);
        return "KIRALed " + valuesMap["LED"] + " Intensity Set To " + valuesMap["Intensity"];
    }
    else
    {
        std::cerr << __PRETTY_FUNCTION__ << " Can't recognize message: " << buffer << ". Aborting..." << std::endl;
        abort();
    }

    if(running_ || paused_) // We go through here after start and resume or
                            // pause: sending back current status
    { std::cout << "Getting time and status here" << std::endl; }

    return "Didn't understand the message!";
}

//========================================================================================================================
std::string PowerSupplyInterface::getTimeStamp()
{
    time_t     rawtime;
    struct tm* timeinfo;
    char       buffer[80];

    time(&rawtime);
    timeinfo = localtime(&rawtime);

    strftime(buffer, sizeof(buffer), "%Y-%m-%d %H:%M:%S", timeinfo);
    std::string str(buffer);
    return str;
}

/*!
************************************************
 * Utility to split strings based on given
 * separator.
 \param line String to be splitted.
 \param separator Separator character used to
 split strings (default is ',')
 \return Returns a vector with splitted strings.
************************************************
*/
std::vector<std::string> PowerSupplyInterface::splitString(const std::string& line, const char& separator)
{
    std::stringstream        buffer(line);
    std::string              aux;
    std::vector<std::string> splittedString;

    while(std::getline(buffer, aux, separator)) splittedString.push_back(aux);
    return splittedString;
}

/*!
************************************************
 * Creates a variable/value map starting from
 * command string and based on a given
 * separator.
 \param line Command input string.
 \param separator Separator used to create
 the map (default value is ':')
 \return Returns a map with variable
 name/variable value.
************************************************
*/
std::map<std::string, std::string> PowerSupplyInterface::createVariablesMap(const std::string& line, const char& separator)
{
    std::map<std::string, std::string> variablesMap;
    const std::vector<std::string>     splittedCommandString = splitString(line);
    variablesMap["Header"]                                   = splittedCommandString[0];
    for(unsigned int v = 1; v < splittedCommandString.size(); ++v)
    {
        std::vector<std::string> splitCommand = splitString(splittedCommandString[v], separator);
        if(splitCommand.size() != 2)
        {
            std::stringstream error;
            error << "Bad command variable value formatting for \"" << splittedCommandString[v] << "\" in command line: \"" << line << "\"";
            throw std::runtime_error(error.str());
        }
        const std::string key = eraseFirstBlankCharacter(splitCommand[0]);
        const std::string val = eraseFirstBlankCharacter(splitCommand[1]);
        variablesMap[key]     = val;
    }
    return variablesMap;
}

/*!
************************************************
 * Strips first char from a string if blank.
 \param str String to be transformed.
 \return Transformed string.
************************************************
*/
std::string PowerSupplyInterface::eraseFirstBlankCharacter(const std::string& str)
{
    if(str.substr(0, 1) == " ")
        return str.substr(1, str.size());
    else
        return str;
}

/*!
************************************************
 * Gets variable value from buffer.
 \param str Variable name.
 \param buffer Buffer.
 \return Variable value string.
************************************************
*/

std::string PowerSupplyInterface::getVariableValue(std::string variable, std::string buffer)
{
    size_t begin = buffer.find(variable) + variable.size() + 1;
    size_t end   = buffer.find(',', begin);
    if(end == std::string::npos) end = buffer.size();
    // std::cout << end << "\t" << begin << std::endl;
    // std::cout << end - begin << std::endl;
    // std::cout << buffer.substr(begin, end - begin) << std::endl;

    if(end > begin) { return buffer.substr(begin, end - begin); }
    else
    {
        return "";
    }
}
