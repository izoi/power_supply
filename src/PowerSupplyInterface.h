#ifndef _PowerSupplyInterface_h_
#define _PowerSupplyInterface_h_

#include "../NetworkUtils/TCPServer.h"
#include "Arduino.h"
#include "DeviceHandler.h"
#include "PowerSupply.h"
#include "PowerSupplyChannel.h"
#include "Scope.h"
#include "ScopeAgilent.h"

#include <mutex>
#include <sstream>
#include <string>
#include <thread>
#include <vector>

class PowerSupplyInterface : public TCPServer
{
  public:
    PowerSupplyInterface(int serverPort, std::string configFileName);
    PowerSupplyInterface(int serverPort);
    virtual ~PowerSupplyInterface(void);

    std::string interpretMessage(const std::string& buffer) override;

  private:
    std::vector<std::string>           splitString(const std::string& line, const char& separator = ',');
    std::string                        eraseFirstBlankCharacter(const std::string& str);
    std::map<std::string, std::string> createVariablesMap(const std::string& line, const char& separator = ':');
    std::string                        getVariableValue(std::string variable, std::string buffer);

    std::string        currentRun_ = "0";
    bool               running_    = false;
    bool               paused_     = false;
    DeviceHandler      fHandler;
    pugi::xml_document fDocSettings;

    std::string getTimeStamp();

    std::mutex fMutex;
};

#endif
