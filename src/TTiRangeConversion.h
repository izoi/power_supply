#ifndef TTIRANGECONVERSION_H
#define TTIRANGECONVERSION_H

#include <string>

static std::string convertRangeMX(int rangeN)
{
    switch(rangeN)
    {
    case 1:
        return "Channel 1: 30V/6A\n"
               "Channel 2: 30V/6A\n"
               "Channel 3: 5.5V/3A";
    case 2:
        return "Channel 1: 15V/10A\n"
               "Channel 2: 15V/10A\n"
               "Channel 3: 12V/1.5A";
    case 3:
        return "Channel 1: 60V/3A\n"
               "Channel 2: 60V/3A";
    case 4: return "Channel 1: 30V/12A";
    case 5: return "Channel 1: 15V/20A";
    case 6: return "Channel 1: 60V/6A";
    case 7: return "Channel 1: 120V/3A";
    default: return std::string("Error ") + std::to_string(rangeN) + " not recognized.";
    }
}

static std::string convertRangeQL(int rangeN)
{
    switch(rangeN)
    {
    case 0:
        return "QL355 Models: 15V/5A\n"
               "QL564 Models: 25V/4A";
    case 1:
        return "QL355 Models: 35V/3A\n"
               "QL564 Models: 56V/2A";
    case 2:
        return "QL355 Models: 35V/500mA\n"
               "QL564 Models: 56V/500mA";
    default: return std::string("Error ") + std::to_string(rangeN) + " not recognized.";
    }
}

static std::string convertRangePL(int rangeN)
{
    switch(rangeN)
    {
    case 1:
        return "LOW CURRENT RANGE\n"
               "PL 068: 6V/800mA\n"
               "PL 155: 15V/500mA\n"
               "PL 303: 30V/500mA\n"
               "PL 601: 60V/500mA\n"
               "PL303QMD: 30V/500mA\n"
               "PL303QMT: 6V/800mA";
    case 2:
        return "HIGH CURRENT RANGE\n"
               "PL 068: 6V/8A\n"
               "PL 155: 15V/5A\n"
               "PL 303: 30V/3A\n"
               "PL 601: 60V/1.5A\n"
               "PL303QMD: 30V/3A\n"
               "PL303QMT: 6V/8A";
    default: return std::string("Error ") + std::to_string(rangeN) + " not recognized.";
    }
}
#endif
