if(NOT DEFINED ENV{OTSDAQ_CMSTRACKER_DIR})
   #includes
   include_directories(${PROJECT_SOURCE_DIR}/src)
   #initial set of libraries
   #find source files
   file(GLOB HEADERS *.h)
   file(GLOB SOURCES *.cc)

   #message(STATUS ${SOURCES})
   #add the library
   add_library(PowerSupply_tools SHARED ${SOURCES} ${HEADERS})
   TARGET_LINK_LIBRARIES(PowerSupply_tools ${LIBS})

else() # ------------------------------- Compilation in the otsdaq environment ---------------------------------------------

   #initial set of libraries
   set(LIBS pugixml boost_system boost_program_options)

   cet_set_compiler_flags(
      EXTRA_FLAGS -Wno-reorder
   )

   cet_make(LIBRARY_NAME Ph2_PowerSupplies_tools
            LIBRARIES
            pthread
            ${LIBS}
            #${Boost_SYSTEM_LIBRARY}
            EXCLUDE ${EXCLUDE_LIST}
   )

   install_headers()
   install_source()
endif()
