/*!
 * \authors Antonio Cassese <antonio.cassese@cern.ch>, INFN-Firenze
 * \date Dec 17 2021
 */

#include "ITIVTools.h"

/*!
************************************************
* Class constructor for Inner Tracker IV
* curve tool.
************************************************
*/
ITIVTools::ITIVTools(std::string configFile, bool systemTestFlag, bool verbose)
{
    std::cout << "Initializing ..." << std::endl;

    fSaveFile       = new std::ofstream;
    fVerbose        = verbose;
    fSystemTestFlag = systemTestFlag;
    fConfigPath     = configFile;
    ReadConfigFile();
}

/*!
************************************************
* Reads config file and exctract proper infos.
************************************************
*/
void ITIVTools::ReadConfigFile()
{
    pugi::xml_document settings;
    if(fConfigPath == "default") fConfigPath = "config/ivITsldo.xml";
    std::cout << "fConfigPath: " << fConfigPath << std::endl;

    fDeviceHandler.readSettings(fConfigPath, settings);

    pugi::xml_node    testNode               = settings.child("IVCurve");
    pugi::xml_node    saveFileNode           = testNode.child("Save");
    pugi::xml_node    analysisNode           = testNode.child("Analysis");
    pugi::xml_node    instrumentNode         = testNode.child("Devices");
    const std::string curveType              = ChildValClean(testNode.child("CurveType").child_value());
    const std::string strSearchTPNode        = "//TestPoints[@Type='" + curveType + "']";
    pugi::xml_node    testPointNode          = testNode.select_node(strSearchTPNode.c_str()).node();
    pugi::xml_node    settingNode            = testPointNode.child("Settings");
    pugi::xml_node    multimeterReadTypeNode = testNode.child(instrumentNode.child("Multimeter").attribute("ReadType").as_string());
    pugi::xml_node    multimeter             = instrumentNode.child("Multimeter");
    pugi::xml_node    powerSupply            = instrumentNode.child("PowerSupply");

    fPlotFlag              = saveFileNode.attribute("PlotFlag").as_bool();
    fMinFit                = saveFileNode.attribute("Minfit").as_string();
    fMaxFit                = saveFileNode.attribute("Maxfit").as_string();
    fRextD                 = saveFileNode.attribute("RextD").as_string();
    fRextA                 = saveFileNode.attribute("RextA").as_string();
    fSaveDir               = saveFileNode.attribute("path").as_string();
    fSaveFileName          = saveFileNode.attribute("file").as_string();
    fKfactor               = analysisNode.attribute("kFactor").as_bool();
    vScanLow               = settingNode.attribute("Lower").as_double();
    vScanHigh              = settingNode.attribute("Higher").as_double();
    vScanStep              = settingNode.attribute("StepSize").as_double();
    vDirection             = settingNode.attribute("Direction").as_string();
    fCompliance            = settingNode.attribute("Compliance").as_float();
    fProtection            = settingNode.attribute("Protection").as_float();
    fEndPoint              = settingNode.attribute("EndPoint").as_float();
    fEndOffPowerSupply     = settingNode.attribute("EndSwitchOff").as_bool();
    fFlagZero              = settingNode.attribute("Zeroing").as_string();
    fMultimeterConfigFile  = multimeter.attribute("ConfigFile").as_string();
    fMultimeterClassName   = multimeter.attribute("ClassName").as_string();
    fReadTypeStr           = multimeter.attribute("ReadType").as_string();
    fMultimeterID          = multimeter.attribute("ID").as_string();
    fPowerSupplyConfigFile = powerSupply.attribute("ConfigFile").as_string();
    fTwoChannelsFlag       = powerSupply.attribute("TwoChannels").as_bool();
    fPowerSupplyID         = powerSupply.attribute("ID").as_string();
    fChannelDigitalID      = powerSupply.attribute("ChannelID_D").as_string();
    fChannelAnalogID       = powerSupply.attribute("ChannelID_A").as_string();
    fScannerCardChNumber   = multimeterReadTypeNode.attribute("nChannels").as_int();
    fCurveType             = curveType;

    if(fVerbose) std::cout << "Creating scanner card channels map and header" << std::endl;
    CreateScannerCardMap(&multimeterReadTypeNode, fScannerCardChannelMap, fVerbose);
}

/*! \brief Run IV curve test.
************************************************
* Run IV curve test for inner tracker..
************************************************
*/
void ITIVTools::RunIV()
{
    DeviceHandler mulHandler;
    DeviceHandler psHandler;

    try
    {
        ConfigureSaving();
        PrepareFileHeaderScanner();
        ConfigureInstruments(&mulHandler, &psHandler);
        StartMultimeter();
        StartPowerSupply();
        StartAcquisition();
        EndAcquisition();
        if(fSaveFile != NULL && fSaveFile->is_open())
        {
            if(fVerbose) std::cout << "Closing save data file..." << std::endl;
            fSaveFile->close();
            CreateLastScanFile();
            RunAnalysis();
        }
    }
    catch(const std::out_of_range& oor)
    {
        std::cerr << "Out of Range error: " << oor.what() << '\n';
    }
}

/*!
************************************************
* Copies data file to LastScan.csv.
************************************************
*/
void ITIVTools::CreateLastScanFile()
{
    std::cout << ("Copying " + fSaveCompleteFileName + " to " + fSaveDir + "lastScan.csv").c_str() << std::endl;
    sleep(1);
    boost::filesystem::copy_file(fSaveCompleteFileName.c_str(),                      // From
                                 (fSaveDir + "lastScan.csv").c_str(),                // to
                                 boost::filesystem::copy_option::overwrite_if_exists // Copy options: overwrite if it exist
    );
}

/*!
************************************************
* Runs python analysis.
************************************************
*/
void ITIVTools::RunAnalysis()
{
    if(fPlotFlag)
    {
        std::cout << "Execution of python script for matplotlib plot of IV" << std::endl;
        // std::string cmd = "python3 scripts/analysisIVscanCROC.py -c " + fConfigPath + " -d " + fSaveDir;
        std::string cmd = "python3 -m analysisIVscanCROC -c " + fConfigPath + " -d " + fSaveDir;
        std::cout << "Executing: " << cmd << std::endl;
        int v = system(cmd.c_str());
        std::cout << "System return value after execution " << v << std::endl;

        size_t      lastIndex    = fSaveFileNameImages.find_last_of(".");
        std::string baseFileName = fSaveFileNameImages.substr(0, lastIndex);

        boost::filesystem::copy_file((fSaveDir + "Images/lastScan.png"), (baseFileName + ".png").c_str());
        boost::filesystem::copy_file((fSaveDir + "Images/lastScan.pdf"), (baseFileName + ".pdf").c_str());
        if(fKfactor && fTwoChannelsFlag)
        {
            boost::filesystem::copy_file((fSaveDir + "Images/lastScan_kfactorD.png"), (baseFileName + "_kfactorD.png").c_str());
            boost::filesystem::copy_file((fSaveDir + "Images/lastScan_kfactorA.png"), (baseFileName + "_kfactorA.png").c_str());
            boost::filesystem::copy_file((fSaveDir + "Images/lastScan_kfactorD.pdf"), (baseFileName + "_kfactorD.pdf").c_str());
            boost::filesystem::copy_file((fSaveDir + "Images/lastScan_kfactorA.pdf"), (baseFileName + "_kfactorA.pdf").c_str());
        }
    }
}

/*! \brief Save parameters configuration.
 ************************************************
 * Creates directory and file streamer from
 * settings in config file.
 \param saveNode Node with saving settings.
************************************************
*/
void ITIVTools::ConfigureSaving()
{
    const std::string saveFileName = fSaveFileName;
    time_t            rawtime;
    struct tm*        timeinfo;
    char              dateString[15];
    char              timeString[15];
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    strftime(dateString, sizeof(dateString), "%Y_%m_%d/", timeinfo);
    strftime(timeString, sizeof(timeString), "%H_%M_%S_", timeinfo);
    fSaveDir += dateString;
    boost::filesystem::path dir(fSaveDir.c_str());
    boost::filesystem::create_directories(dir);
    fSaveCompleteFileName = fSaveDir + timeString + saveFileName;
    fSaveFileName         = timeString + saveFileName;
    fSaveFileNameImages   = fSaveDir + "Images/" + timeString + saveFileName;
    if(fVerbose) std::cout << "\nOpening file: " << fSaveCompleteFileName << " ..." << std::endl;
    fSaveFile->open(fSaveCompleteFileName, std::ofstream::out | std::ofstream::app);
    if(fVerbose) std::cout << "File is open: " << fSaveFile->is_open() << std::endl << std::endl;
}

/*! \brief Instruments configuration.
************************************************
 * Configure instruments described in xml file
 * and specified in instruments config file(s).
 list.
************************************************
*/
void ITIVTools::ConfigureInstruments(DeviceHandler* mulHandler, DeviceHandler* psHandler)
{
    pugi::xml_document multimeterDocSettings;
    pugi::xml_document powerSupplyDocSettings;
    if(fVerbose)
    {
        std::cout << "fMultimeterClassName: " << fMultimeterClassName << std::endl;
        std::cout << "Multimeter configuration file is: " << fMultimeterConfigFile << std::endl;
        std::cout << "Power supply configuration file is: " << fPowerSupplyConfigFile << std::endl;
    }
    mulHandler->readSettings(fMultimeterConfigFile, multimeterDocSettings, fVerbose);
    psHandler->readSettings(fPowerSupplyConfigFile, powerSupplyDocSettings, fVerbose);

    if(fMultimeterClassName == "KeithleyMultimeter")
        fKeithleyMultimeter = dynamic_cast<KeithleyMultimeter*>(mulHandler->getMultimeter(fMultimeterID));
    else
    {
        std::stringstream error;
        error << "Code not implemented for " << fMultimeterClassName << " multimeter type, aborting...";
        throw std::runtime_error(error.str());
    }
    fPowerSupply = psHandler->getPowerSupply(fPowerSupplyID);
    fPowerSupplyChannel.push_back(fPowerSupply->getChannel(fChannelDigitalID));
    fPowerSupplyChannel.push_back(fPowerSupply->getChannel(fChannelAnalogID));
}

/*! \brief Setting file header.
************************************************
* Writes the file header starting from channel
* map informations.
************************************************
*/
void ITIVTools::PrepareFileHeaderScanner()
{
    std::cout << "nChannels: " << fScannerCardChNumber << std::endl;
    *fSaveFile << "CurrentD"
               << ",VpsD"
               << ",CurrentA"
               << ",VpsA";
    for(int v = 1; v < fScannerCardChNumber + 1; ++v)
    {
        if(fScannerCardChannelMap.count(v) != 1)
        {
            // std::cout << "Entering here: " << v << std::endl;
            *fSaveFile << ",";
            if(fScannerCardChannelMap.count(v) > 1) std::cout << "More than one channel_" << v << " defined, please check config file" << std::endl;
            continue;
        }
        *fSaveFile << "," << fScannerCardChannelMap[v];
    }
    *fSaveFile << std::endl;
}

/*! \brief Data acquisition vector preparation.
************************************************
 * Fills vector for data acquisition with the
 * right direction.
 \param data Vector pointer for acquisition
 data points filling.
 \param first First point.
 \param last Last point.
 \param stepSize Step size between points.
************************************************
*/
void ITIVTools::FillScanPointsVector(std::vector<double>* data, const double first, const double last, const double stepSize)
{
    double step;
    int    nStep;
    step = fabs(last - first);
    step /= fabs(stepSize);
    nStep = (int)std::floor(step);
    nStep += 1;
    for(int v = 0; v < nStep; ++v) data->push_back(first + v * stepSize);
    if(data->back() != last) data->push_back(last);
}

/*!
************************************************
 * Creates data taking point starting from
 * config file.
 \return Returns vector with data taking
 points.
************************************************
*/
std::vector<double> ITIVTools::PrepareScanPoints()
{
    std::vector<double> dataTakingPoints;

    if(fVerbose) std::cout << "Direction: " << vDirection << std::endl;

    if(vDirection == "up")
        FillScanPointsVector(&dataTakingPoints, vScanLow, vScanHigh, vScanStep);
    else if(vDirection == "down")
        FillScanPointsVector(&dataTakingPoints, vScanHigh, vScanLow, -vScanStep);
    else if(vDirection == "both")
    {
        FillScanPointsVector(&dataTakingPoints, vScanLow, vScanHigh, vScanStep);
        FillScanPointsVector(&dataTakingPoints, vScanHigh, vScanLow, -vScanStep);
    }
    else
    {
        std::stringstream error;
        error << "Direction: " << vDirection << " in configuration file for IT IV curve not defined";
        throw std::runtime_error(error.str());
    }
    return dataTakingPoints;
}

/*! \brief Multimeter preparation.
************************************************
 * Performs starting routines on multimeter.
 \return True if everything works.
************************************************
*/
void ITIVTools::StartMultimeter()
{
    if(fMultimeterClassName == "KeithleyMultimeter")
    {
        fKeithleyMultimeter->reset();
        fKeithleyMultimeter->enableInternalScan();
    }
}

/*! Powersupply preparation.
************************************************
 * Performs starting routines on power supply.
 \return True if everything works.
************************************************
*/
void ITIVTools::StartPowerSupply()
{
    // fPowerSupply->reset();
    // sleep(1);
    if(fCurveType == "Current")
    {
        for(unsigned int i = 0; i < fPowerSupplyChannel.size(); i++)
        {
            fPowerSupplyChannel[i]->setOverVoltageProtection(fProtection);
            fPowerSupplyChannel[i]->setVoltageCompliance(fCompliance);
            fPowerSupplyChannel[i]->setCurrent(0);
        }
    }
    else if(fCurveType == "Voltage")
    {
        for(unsigned int i = 0; i < fPowerSupplyChannel.size(); i++)
        {
            fPowerSupplyChannel[i]->setOverCurrentProtection(fProtection);
            fPowerSupplyChannel[i]->setCurrentCompliance(fCompliance);
            fPowerSupplyChannel[i]->setVoltage(0);
        }
    }
    else
    {
        std::stringstream error;
        error << "Code not implemented for " << fCurveType << " curve type, aborting...";
        throw std::runtime_error(error.str());
    }
    for(unsigned int v = 0; v < fPowerSupplyChannel.size(); ++v)
        if(!fPowerSupplyChannel[v]->isOn()) fPowerSupplyChannel[v]->turnOn();
}

/*! \brief Scanner card acquisition.
************************************************
 * Acquisition for multimeter with scanner
 * card.
 \return bool returns true if acquisition
 worked properly.
************************************************
*/
void ITIVTools::ScannerCardAcquisition()
{
    try
    {
        std::vector<double> vScan        = PrepareScanPoints();
        int                 firstChannel = fScannerCardChannelMap.begin()->first;
        int                 lastChannel  = fScannerCardChannelMap.end()->first;
        std::cout << "First channel: " << firstChannel << std::endl << "Last channel: " << lastChannel << std::endl;
        for(unsigned int v = 0; v < vScan.size(); ++v)
        {
            if(fVerbose) std::cout << "ScanPoint: " << vScan[v] << std::endl;
            if(vScan[v] > 0.01)
            {
                for(unsigned int i = 0; i < fPowerSupplyChannel.size(); i++) { fPowerSupplyChannel[i]->setCurrent(vScan[v]); }
            }
            else
            {
                for(unsigned int i = 0; i < fPowerSupplyChannel.size(); i++) { fPowerSupplyChannel[i]->setCurrent(0.01); }
            }
            sleep(1); // timeout s
            for(unsigned int v = 0; v < fPowerSupplyChannel.size(); ++v) *fSaveFile << fPowerSupplyChannel[v]->getCurrent() << "," << fPowerSupplyChannel[v]->getOutputVoltage() << ",";
            std::string readStr = fKeithleyMultimeter->scanChannels(firstChannel, lastChannel);
            if(readStr.back() != '\n') readStr.push_back('\n');
            if(fVerbose) std::cout << "Output from keithley is: " << readStr;
            *fSaveFile << readStr;
            if(fFlagZero == "no" || fFlagZero == "No" || fFlagZero == "NO") continue;
            for(unsigned int v = 0; v < fPowerSupplyChannel.size(); ++v) fPowerSupplyChannel[v]->setCurrent(0.01); // Sets current to zero
            sleep(1);
            if(fVerbose)
            {
                std::cout << "Zeroing current" << std::endl;
                for(unsigned int v = 0; v < fPowerSupplyChannel.size(); ++v)
                {
                    std::cout << "Current value after zeroing: " << fPowerSupplyChannel[v]->getCurrent() << " A" << std::endl;
                    std::cout << "Voltage value after zeroing: " << fPowerSupplyChannel[v]->getOutputVoltage() << " V" << std::endl;
                }
            }
        }
        if(!fSystemTestFlag)
            for(unsigned int v = 0; v < fPowerSupplyChannel.size(); ++v) fPowerSupplyChannel[v]->turnOff();
    }
    catch(const std::out_of_range& oor)
    {
        std::cerr << "Out of Range error: " << oor.what() << '\n';
    }
}

/*! \brief Data acquisition.
************************************************
* Starts the acquisition of data.
************************************************
*/
void ITIVTools::StartAcquisition()
{
    if(fReadTypeStr == "ScannerCard")
        ScannerCardAcquisition();
    else
    {
        std::stringstream error;
        error << "Code not implemented for " << fReadTypeStr << " multimeter reading type, aborting...";
        throw std::runtime_error(error.str());
    }
}

/*!
************************************************
* Performs operation on PS after acquisition
* of points.
************************************************
*/
void ITIVTools::EndAcquisition()
{
    if(fVerbose) std::cout << "Setting Power supply channel(s) to: " << fEndPoint << " A" << std::endl;
    for(unsigned int v = 0; v < fPowerSupplyChannel.size(); v++) { fPowerSupplyChannel[v]->setCurrent(fEndPoint); }
    if(fEndOffPowerSupply)
        for(unsigned int v = 0; v < fPowerSupplyChannel.size(); v++) { fPowerSupplyChannel[v]->turnOff(); }
}
