/*!
 * \authors Antonio Cassese <antonio.cassese@cern.ch>, INFN-Firenze
 * \date Dec 17 2021
 */

#ifndef ITIVTools_H
#define ITIVTools_H

#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp> //!For command line arg parsing
#include <cmath>
#include <cstdlib>
#include <errno.h>
#include <fstream>
//#include <iomanip>
//#include <iostream>
//#include <limits>
//#include <pugixml.hpp>
//#include <sstream>
//#include <string>
//#include <vector>

#include "../src/DeviceHandler.h"
#include "../src/KeithleyMultimeter.h"
#include "../src/Multimeter.h"
#include "../src/PowerSupply.h"
#include "../src/PowerSupplyChannel.h"

#include "Tools.h"

/*!
************************************************
 \class ITIVTools.
 \brief Inner Tracker tool for IV curve..
************************************************
*/

class ITIVTools : public Tools
{
  public:
    ITIVTools(std::string configFile, bool systemTestFlag, bool verbose = false);
    void                ReadConfigFile();
    void                RunIV();
    void                CreateLastScanFile();
    void                RunAnalysis();
    void                ConfigureSaving();
    void                ConfigureInstruments(DeviceHandler*, DeviceHandler*);
    void                PrepareFileHeaderScanner();
    void                FillScanPointsVector(std::vector<double>*, const double, const double, const double);
    std::vector<double> PrepareScanPoints();
    void                StartMultimeter();
    void                StartPowerSupply();
    void                ScannerCardAcquisition();
    void                StartAcquisition();
    void                EndAcquisition();

  private:
    PowerSupply*                     fPowerSupply;
    std::vector<PowerSupplyChannel*> fPowerSupplyChannel;
    KeithleyMultimeter*              fKeithleyMultimeter;
    DeviceHandler                    fDeviceHandler;
    bool                             fVerbose;
    bool                             fTwoChannelsFlag;
    bool                             fSystemTestFlag;
    bool                             fPlotFlag;
    bool                             fKfactor;
    bool                             fEndOffPowerSupply;
    int                              fScannerCardChNumber;
    float                            fCompliance;
    float                            fProtection;
    float                            fEndPoint;
    double                           vScanLow;
    double                           vScanHigh;
    double                           vScanStep;
    std::string                      vDirection;
    std::string                      fFlagZero;
    std::string                      fMinFit;
    std::string                      fMaxFit;
    std::string                      fRextD;
    std::string                      fRextA;
    std::string                      fCurveType;
    std::string                      fConfigPath;
    std::string                      fMultimeterClassName;
    std::string                      fSaveFileName;
    std::string                      fSaveCompleteFileName;
    std::string                      fSaveFileNameImages;
    std::string                      fSaveDir;
    std::string                      fMultimeterConfigFile;
    std::string                      fPowerSupplyConfigFile;
    std::string                      fReadTypeStr;
    std::string                      fMultimeterID;
    std::string                      fPowerSupplyID;
    std::string                      fChannelDigitalID;
    std::string                      fChannelAnalogID;
    std::ofstream*                   fSaveFile;
    std::map<int, std::string>       fScannerCardChannelMap;
    std::string                      fSaveFileNameMux;

  public: // Variables access methods
    // Set
    void SetPlotFlag(const bool val) { fPlotFlag = val; };
    void SetKFactor(const bool val) { fKfactor = val; };
    void SetEndOffPowerSupply(const bool val) { fEndOffPowerSupply = val; };
    void SetMinFit(const std::string val) { fMinFit = val; };
    void SetMaxFit(const std::string val) { fMaxFit = val; };
    void SetRExtD(const std::string val) { fRextD = val; };
    void SetRExtA(const std::string val) { fRextA = val; };
    void SetCurveType(const std::string val) { fCurveType = val; };
    void SetSaveDir(const std::string val) { fSaveDir = val; };
    void SetSaveFileName(const std::string val) { fSaveFileName = val; }
    void SetSaveCompleteFileName(const std::string val) { fSaveCompleteFileName = val; }
    void SetDirection(const std::string val) { vDirection = val; };
    void SetFlagZero(const std::string val) { fFlagZero = val; };
    void SetScanLow(const double val) { vScanLow = val; }
    void SetScanHigh(const double val) { vScanHigh = val; }
    void SetScanStep(const double val) { vScanStep = val; }
    void SetCompliance(const float val) { fCompliance = val; };
    void SetProtection(const float val) { fProtection = val; };
    void SetEndPoint(const float val) { fEndPoint = val; };
    void SetSaveFileNameMux(const std::string val) { fSaveFileNameMux = val; };
    // Get
    bool                       GetPlotFlag() { return fPlotFlag; }
    bool                       GetKFactor() { return fKfactor; };
    bool                       GetEndOffPowerSupply() { return fEndOffPowerSupply; };
    std::string                GetMinFit() { return fMinFit; };
    std::string                GetMaxFit() { return fMaxFit; };
    std::string                GetRExtD() { return fRextD; };
    std::string                GetRExtA() { return fRextA; };
    std::string                GetCurveType() { return fCurveType; };
    std::string                GetSaveDir() { return fSaveDir; };
    std::string                GetSaveFileName() { return fSaveFileName; };
    std::string                GetSaveCompleteFileName() { return fSaveCompleteFileName; };
    std::string                GetDirection() { return vDirection; };
    std::string                GetFlagZero() { return fFlagZero; };
    double                     GetScanLow() { return vScanLow; };
    double                     GetScanHigh() { return vScanHigh; };
    double                     GetScanStep() { return vScanStep; };
    float                      GetCompliance() { return fCompliance; };
    float                      GetProtection() { return fProtection; };
    float                      GetEndPoint() { return fEndPoint; };
    std::map<int, std::string> GetScannerCardMap() { return fScannerCardChannelMap; };
    std::ofstream*             GetSaveFileDescriptor() { return fSaveFile; };
    std::string                GetSaveFileNameMux() { return fSaveFileNameMux; };
};

#endif
